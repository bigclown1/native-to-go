import {
  styleValid,
  styleInvalid,
  linkStyle,
  toolTip,
  toolTipIcon,
  titleStyle
} from './colors'
import { noPadding, flexC, flexR, textCenter, padding } from './display'
import { small, smallAlt } from './size'

export { styleValid, styleInvalid, linkStyle, toolTip, toolTipIcon, titleStyle }
export { noPadding, flexC, flexR, textCenter, padding }
export { small, smallAlt }
