const flexC = {
  display: 'flex',
  flexDirection: 'column',
  margin: '0px'
}
const flexR = {
  display: 'inline-flex',
  flexDirection: 'row',
  margin: '0px'
}
const padding = {
  paddingTop: '1.5rem'
}
const noPadding = {
  margin: '0px',
  padding: '0px'
}
const textCenter = {
  textAlign: 'center'
}

export { flexC, flexR, noPadding, textCenter, padding }
