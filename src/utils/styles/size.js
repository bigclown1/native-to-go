const small = {
  fontSize: 'small'
}
const smallAlt = {
  fontSize: '1rem',
  paddingLeft: '5px'
}
export { small, smallAlt }
