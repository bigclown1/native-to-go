const styleValid = {
  width: '100%',
  height: '0px',
  paddingTop: 0.25,
  fontSize: '80%',
  color: '#43a047'
}
const styleInvalid = {
  width: '100%',
  height: '0px',
  paddingTop: 0.25,
  fontSize: '80%',
  color: '#dc3545'
}
const titleStyle = {
  fontSize: '120%',
  fontWeight: '700'
}
const linkStyle = {
  color: '#53A8F8',
  fontSize: '80%',
  textDecoration: 'underline'
}

const toolTip = {
  backgroundColor: '#effefd',
  fontSize: '90%',
  color: 'black',
  border: 'solid',
  borderWidth: 'thin',
  borderRadius: '5px',
  fontWeight: '600'
}
const toolTipIcon = {
  color: '#3498db'
}
export { styleInvalid, styleValid, linkStyle, toolTip, toolTipIcon, titleStyle }
