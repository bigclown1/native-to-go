import 'react-native-gesture-handler'
import React from 'react'
import MainNavigation from './components/Navigation/MainStackNavigator'
import { Provider } from 'react-redux'
import configureStore from './store/index'
import { enableScreens } from 'react-native-screens'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import {Text} from 'react-native'
enableScreens()

const store = configureStore()
export const persistor = persistStore(store)
const App = () => {
  return (
    <>
      <Provider store={store}>
        <PersistGate loading={<Text>Cargando</Text>} persistor={persistor}>
          <MainNavigation />
        </PersistGate>
      </Provider>
    </>
  )
}

export default App
