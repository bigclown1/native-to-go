export const COMPANY_SAVE_REQUESTED = 'COMPANY_SAVE_REQUESTED'
export function requestSaveCompany(
  name,
  regimen,
  cuit,
  tel,
  email,
  address,
  idLocation,
  nameLocation,
  idProvince,
  postalCode,
  inaesCode,
  actionType
) {
  return {
    type: COMPANY_SAVE_REQUESTED,
    name,
    regimen,
    cuit,
    tel,
    email,
    address,
    idLocation,
    nameLocation,
    idProvince,
    postalCode,
    inaesCode,
    actionType
  }
}

export const COMPANY_SAVE_SUCCEEDED = 'COMPANY_SAVE_SUCCEEDED'
export const companySaveSucceeded = () => ({ type: COMPANY_SAVE_SUCCEEDED })

export const COMPANIES_FETCH_REQUESTED = 'COMPANIES_FETCH_REQUESTED'
export const companyRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: COMPANIES_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const COMPANIES_FETCH_SUCCEEDED = 'COMPANIES_FETCH_SUCCEEDED'
export const receiveCompanies = (result, size, total) => ({
  type: COMPANIES_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const COMPANY_DEACTIVATE_REQUESTED = 'COMPANY_DEACTIVATE_REQUESTED'
export const companyRequestedDeactivate = (
  id,
  name,
  regimen,
  cuit,
  tel,
  email,
  address,
  location,
  active
) => ({
  type: COMPANY_DEACTIVATE_REQUESTED,
  id,
  name,
  regimen,
  cuit,
  tel,
  email,
  address,
  location,
  active
})

export const COMPANY_UPDATE_REQUESTED = 'COMPANY_UPDATE_REQUESTED'
export const companyRequestedUpdate = id => ({
  type: COMPANY_UPDATE_REQUESTED,
  id
})

export const COMPANY_FETCH_SUCCEEDED = 'COMPANY_FETCH_SUCCEEDED'
export const receiveCompany = result => ({
  type: COMPANY_FETCH_SUCCEEDED,
  result
})

export const COMPANIES_ACTIVE_FETCH_REQUESTED =
  'COMPANIES_ACTIVE_FETCH_REQUESTED'
export const companiesRequestedFetch = (fvalue, fname) => ({
  type: COMPANIES_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_COMPANIES_FINDED = 'RECEIVE_COMPANIES_FINDED'
export const receiveCompaniesFinded = result => ({
  type: RECEIVE_COMPANIES_FINDED,
  result
})
