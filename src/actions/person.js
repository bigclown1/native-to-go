export const PERSONS_NOSIS_FETCH_REQUESTED = 'PERSONS_NOSIS_FETCH_REQUESTED'
export const personNosisRequestedFetch = fvalue => ({
  type: PERSONS_NOSIS_FETCH_REQUESTED,
  fvalue
})

export const PERSONS_NOSIS_FETCH_SUCCEEDED = 'PERSONS_NOSIS_FETCH_SUCCEEDED'
export const receiveNosisPersons = (result, size, total) => ({
  type: PERSONS_NOSIS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const PERSON_FETCH_REQUESTED = 'PERSON_FETCH_REQUESTED'
export const personRequestedFetch = (fvalue, fname, toUpdate) => ({
  type: PERSON_FETCH_REQUESTED,
  fvalue,
  fname,
  toUpdate
})

export const PERSONS_FETCH_REQUESTED = 'PERSONS_FETCH_REQUESTED'
export const personsRequestedFetch = (fvalue, fname) => ({
  type: PERSONS_FETCH_REQUESTED,
  fvalue,
  fname
})

export const PERSONS_FETCH_SUCCEEDED = 'PERSONS_FETCH_SUCCEEDED'
export const receivePersons = (result, size, total) => ({
  type: PERSONS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const PERSON_FETCH_SUCCEEDED = 'PERSON_FETCH_SUCCEEDED'
export const receivePerson = result => ({
  type: PERSON_FETCH_SUCCEEDED,
  result
})

export const PERSON_FETCH_TO_UPDATE_SUCCEEDED =
  'PERSON_FETCH_TO_UPDATE_SUCCEEDED'
export const receivePersonToUpdate = result => ({
  type: PERSON_FETCH_TO_UPDATE_SUCCEEDED,
  result
})

export const PERSON_SAVE_REQUESTED = 'PERSON_SAVE_REQUESTED'
export function requestSavePerson(jsonAdd, actionType) {
  return {
    type: PERSON_SAVE_REQUESTED,
    jsonAdd,
    actionType
  }
}

export const PERSON_SAVE_SUCCEEDED = 'PERSON_SAVE_SUCCEEDED'
export const personSaveSucceeded = () => ({ type: PERSON_SAVE_SUCCEEDED })

export const PERSON_UPDATE_REQUESTED = 'PERSON_UPDATE_REQUESTED'
export const personRequestedUpdate = (fvalue, fname, toUpdate) => ({
  type: PERSON_UPDATE_REQUESTED,
  fvalue,
  fname,
  toUpdate
})

export const PERSON_DEACTIVATE_REQUESTED = 'PERSON_DEACTIVATE_REQUESTED'
export const personRequestedDeactivate = jsonDeactivate => ({
  type: PERSON_DEACTIVATE_REQUESTED,
  jsonDeactivate
})
