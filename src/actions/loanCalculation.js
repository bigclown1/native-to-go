export const LOANS_FETCH_REQUESTED = 'LOANS_FETCH_REQUESTED'
export const loanRequestedFetch = loanCalculation => ({
  type: LOANS_FETCH_REQUESTED,
  loanCalculation
})

export const LOANS_FETCHASKED_REQUESTED = 'LOANS_FETCHASKED_REQUESTED'
export const loanAskedRequestedFetch = loanCalculation => ({
  type: LOANS_FETCHASKED_REQUESTED,
  loanCalculation
})

export const LOANS_FETCH_SUCCEEDED = 'LOANS_FETCH_SUCCEEDED'
export const receiveLoan = (result, size, total) => ({
  type: LOANS_FETCH_SUCCEEDED,
  result,
  size,
  total
})
