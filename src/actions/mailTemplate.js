export const MAILTEMPLATE_SAVE_REQUESTED = 'MAILTEMPLATE_SAVE_REQUESTED'
export function requestSaveMailTemplate(name, body, actionType) {
  return {
    type: MAILTEMPLATE_SAVE_REQUESTED,
    name,
    body,
    actionType
  }
}

export const MAILTEMPLATE_SAVE_SUCCEEDED = 'MAILTEMPLATE_SAVE_SUCCEEDED'
export const mailTemplateSaveSucceeded = () => ({
  type: MAILTEMPLATE_SAVE_SUCCEEDED
})

export const MAILTEMPLATES_FETCH_REQUESTED = 'MAILTEMPLATES_FETCH_REQUESTED'
export const mailTemplateRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: MAILTEMPLATES_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const MAILTEMPLATES_FETCH_SUCCEEDED = 'MAILTEMPLATES_FETCH_SUCCEEDED'
export const receiveMailTemplates = (result, size, total) => ({
  type: MAILTEMPLATES_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const MAILTEMPLATE_DEACTIVATE_REQUESTED =
  'MAILTEMPLATE_DEACTIVATE_REQUESTED'
export const mailTemplateRequestedDeactivate = (id, name, body, active) => ({
  type: MAILTEMPLATE_DEACTIVATE_REQUESTED,
  id,
  name,
  body,
  active
})

export const MAILTEMPLATE_UPDATE_REQUESTED = 'MAILTEMPLATE_UPDATE_REQUESTED'
export const mailTemplateRequestedUpdate = id => ({
  type: MAILTEMPLATE_UPDATE_REQUESTED,
  id
})

export const MAILTEMPLATE_FETCH_SUCCEEDED = 'MAILTEMPLATE_FETCH_SUCCEEDED'
export const receiveMailTemplate = result => ({
  type: MAILTEMPLATE_FETCH_SUCCEEDED,
  result
})
