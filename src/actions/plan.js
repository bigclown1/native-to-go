export const PLAN_SAVE_REQUESTED = 'PLAN_SAVE_REQUESTED'
export function requestSavePlan(jsonAdd, actionType) {
  return { type: PLAN_SAVE_REQUESTED, jsonAdd, actionType }
}

export const PLAN_SAVE_SUCCEEDED = 'PLAN_SAVE_SUCCEEDED'
export const planSaveSucceeded = () => ({ type: PLAN_SAVE_SUCCEEDED })

export const PLANS_FETCH_REQUESTED = 'PLANS_FETCH_REQUESTED'
export const planRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: PLANS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const PLANS_FETCH_SUCCEEDED = 'PLANS_FETCH_SUCCEEDED'
export const receivePlans = (result, size, total) => ({
  type: PLANS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const PLAN_DEACTIVATE_REQUESTED = 'PLAN_DEACTIVATE_REQUESTED'
export const planRequestedDeactivate = (id, number, active) => ({
  type: PLAN_DEACTIVATE_REQUESTED,
  id,
  number,
  active
})

export const PLAN_UPDATE_REQUESTED = 'PLAN_UPDATE_REQUESTED'
export const planRequestedUpdate = id => ({
  type: PLAN_UPDATE_REQUESTED,
  id
})

export const PLAN_FETCH_SUCCEEDED = 'PLAN_FETCH_SUCCEEDED'
export const receivePlan = result => ({
  type: PLAN_FETCH_SUCCEEDED,
  result
})

export const PLANS_ACTIVE_FETCH_REQUESTED = 'PLANS_ACTIVE_FETCH_REQUESTED'
export const plansRequestedFetch = (fvalue, fname) => ({
  type: PLANS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_PLAN_FINDED = 'RECEIVE_PLAN_FINDED'
export const receivePlanFinded = result => ({
  type: RECEIVE_PLAN_FINDED,
  result
})
