export const CONFIGQUERY_SAVE_REQUESTED = 'CONFIGQUERY_SAVE_REQUESTED'
export function requestSaveConfigQuery(name, value) {
  return {
    type: CONFIGQUERY_SAVE_REQUESTED,
    name,
    value
  }
}

export const CONFIGQUERY_SAVE_SUCCEEDED = 'CONFIGQUERY_SAVE_SUCCEEDED'
export const configQuerySaveSucceeded = (result, size, total) => ({
  type: CONFIGQUERY_SAVE_SUCCEEDED,
  result,
  size,
  total
})

export const CONFIGQUERY_FETCH_REQUESTED = 'CONFIGQUERY_FETCH_REQUESTED'
export const configQueryRequestedFetch = () => ({
  type: CONFIGQUERY_FETCH_REQUESTED
})

export const CONFIGQUERY_FETCH_SUCCEEDED = 'CONFIGQUERY_FETCH_SUCCEEDED'
export const receiveConfigQuerys = (result, size, total) => ({
  type: CONFIGQUERY_FETCH_SUCCEEDED,
  result,
  size,
  total
})
