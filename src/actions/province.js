export const PROVINCES_ACTIVE_FETCH_REQUESTED =
  'PROVINCES_ACTIVE_FETCH_REQUESTED'
export const provincesRequestedFetch = (fvalue, fname) => ({
  type: PROVINCES_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_PROVINCE_FINDED = 'RECEIVE_PROVINCE_FINDED'
export const receiveProvinceFinded = result => ({
  type: RECEIVE_PROVINCE_FINDED,
  result
})
