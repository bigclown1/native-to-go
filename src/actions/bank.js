export const BANK_SAVE_REQUESTED = 'BANK_SAVE_REQUESTED'
export function requestSaveBank(jsonAdd, actionType) {
  return {
    type: BANK_SAVE_REQUESTED,
    jsonAdd,
    actionType
  }
}

export const BANK_SAVE_SUCCEEDED = 'BANK_SAVE_SUCCEEDED'
export const bankSaveSucceeded = () => ({ type: BANK_SAVE_SUCCEEDED })

export const BANKS_FETCH_REQUESTED = 'BANKS_FETCH_REQUESTED'
export const bankRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: BANKS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const BANKS_FETCH_SUCCEEDED = 'BANKS_FETCH_SUCCEEDED'
export const receiveBanks = (result, size, total) => ({
  type: BANKS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const BANK_DEACTIVATE_REQUESTED = 'BANK_DEACTIVATE_REQUESTED'
export const bankRequestedDeactivate = (id, number, active) => ({
  type: BANK_DEACTIVATE_REQUESTED,
  id,
  number,
  active
})

export const BANK_UPDATE_REQUESTED = 'BANK_UPDATE_REQUESTED'
export const bankRequestedUpdate = id => ({
  type: BANK_UPDATE_REQUESTED,
  id
})

export const BANK_FETCH_SUCCEEDED = 'BANK_FETCH_SUCCEEDED'
export const receiveBank = result => ({
  type: BANK_FETCH_SUCCEEDED,
  result
})

export const BANKS_ACTIVE_FETCH_REQUESTED = 'BANKS_ACTIVE_FETCH_REQUESTED'
export const banksRequestedFetch = (fvalue, fname, withNew) => ({
  type: BANKS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname,
  withNew
})

export const RECEIVE_BANK_FINDED = 'RECEIVE_BANK_FINDED'
export const receiveBankFinded = result => ({
  type: RECEIVE_BANK_FINDED,
  result
})
