export const LOANINQUERY_SAVE_REQUESTED = 'LOANINQUERY_SAVE_REQUESTED'
export function requestSaveLoanInQuery(jsonAdd, step, id) {
  return {
    type: LOANINQUERY_SAVE_REQUESTED,
    jsonAdd,
    step,
    id
  }
}

export const LOANINQUERY_UPLOAD_FILES = 'LOANINQUERY_UPLOAD_FILES'
export function requestUploadFilesLoanInQuery(file, typeFiles, id) {
  return {
    type: LOANINQUERY_UPLOAD_FILES,
    file,
    typeFiles,
    id
  }
}

export const LOANINQUERY_DELETE_FILES = 'LOANINQUERY_DELETE_FILES'
export function requestDeleteFilesLoanInQuery(file, id) {
  return {
    type: LOANINQUERY_DELETE_FILES,
    file,
    id
  }
}

export const LOANMESSAGE_SAVE_REQUESTED = 'LOANMESSAGE_SAVE_REQUESTED'
export function requestSaveLoanMessage(jsonMessage, id) {
  return {
    type: LOANMESSAGE_SAVE_REQUESTED,
    jsonMessage,
    id
  }
}
