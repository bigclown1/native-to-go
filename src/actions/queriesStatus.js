export const QUERIES_FETCH_REQUESTED = 'QUERIES_FETCH_REQUESTED'
export const queriesRequestedFetch = (
  idUsuar,
  fvalue,
  fname,
  current,
  columnOrder,
  orderBy
) => ({
  type: QUERIES_FETCH_REQUESTED,
  idUsuar,
  fvalue,
  fname,
  current,
  columnOrder,
  orderBy
})

export const QUERIES_FETCH_SUCCEEDED = 'QUERIES_FETCH_SUCCEEDED'
export const receiveQueries = (result, size, total) => ({
  type: QUERIES_FETCH_SUCCEEDED,
  result,
  size,
  total
})
