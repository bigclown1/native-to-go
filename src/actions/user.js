export const USER_SAVE_REQUESTED = 'USER_SAVE_REQUESTED'
export function requestSaveUser(name, pass, email, role, actionType) {
  return {
    type: USER_SAVE_REQUESTED,
    name,
    pass,
    email,
    role,
    actionType
  }
}

export const USER_SAVE_SUCCEEDED = 'USER_SAVE_SUCCEEDED'
export const userSaveSucceeded = () => ({ type: USER_SAVE_SUCCEEDED })

export const USERS_FETCH_REQUESTED = 'USERS_FETCH_REQUESTED'
export const userRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: USERS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const USERS_FETCH_SUCCEEDED = 'USERS_FETCH_SUCCEEDED'
export const receiveUsers = (result, size, total) => ({
  type: USERS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const USER_DEACTIVATE_REQUESTED = 'USER_DEACTIVATE_REQUESTED'
export const userRequestedDeactivate = (id, role, active) => ({
  type: USER_DEACTIVATE_REQUESTED,
  id,
  role,
  active
})

export const USER_UPDATE_REQUESTED = 'USER_UPDATE_REQUESTED'
export const userRequestedUpdate = id => ({
  type: USER_UPDATE_REQUESTED,
  id
})

export const USER_FETCH_SUCCEEDED = 'USER_FETCH_SUCCEEDED'
export const receiveUser = result => ({
  type: USER_FETCH_SUCCEEDED,
  result
})
export const RECEIVE_USER_FINDED = 'RECEIVE_USER_FINDED'
export const receiveUserFinded = result => ({
  type: RECEIVE_USER_FINDED,
  result
})

export const USER_ACTIVE_FETCH_REQUESTED_FIND =
  'USER_ACTIVE_FETCH_REQUESTED_FIND'
export const userRequestedFetchFind = (fvalue, fname) => ({
  type: USER_ACTIVE_FETCH_REQUESTED_FIND,
  fvalue,
  fname
})
