export const FORM_FETCH_REQUESTED = 'FORM_FETCH_REQUESTED'
export const formRequestedFetch = (fname, fvalue) => ({
  type: FORM_FETCH_REQUESTED,
  fname,
  fvalue
})

export const FORM_FETCH_SUCCEEDED = 'FORM_FETCH_SUCCEEDED'
export const receiveFormSucceeded = (result, size, total) => ({
  type: FORM_FETCH_SUCCEEDED,
  result,
  size,
  total
})
