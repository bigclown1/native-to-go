export const QUERY_SAVE_REQUESTED = 'QUERY_SAVE_REQUESTED'
export function requestSaveQuery(
  fullName,
  dni,
  cuil,
  sex,
  birthDay,
  email,
  nationality,
  maritalStatus,
  idOrganism,
  idBank,
  salary,
  bankFeeCredits,
  cardDebits,
  discovered,
  debits,
  deposits,
  extractions,
  laborOld,
  collectionBankSituation,
  qSituationFiveOrGreater,
  nosisScore,
  amountAsked,
  worstBankSituation,
  maxNumberCommercial
) {
  return {
    type: QUERY_SAVE_REQUESTED,
    fullName,
    dni,
    cuil,
    sex,
    birthDay,
    email,
    nationality,
    maritalStatus,
    idOrganism,
    idBank,
    salary,
    bankFeeCredits,
    cardDebits,
    discovered,
    debits,
    deposits,
    extractions,
    laborOld,
    collectionBankSituation,
    qSituationFiveOrGreater,
    nosisScore,
    amountAsked,
    worstBankSituation,
    maxNumberCommercial
  }
}

export const QUERY_SAVE_SUCCEEDED = 'QUERY_SAVE_SUCCEEDED'
export const querySaveSucceeded = result => ({
  type: QUERY_SAVE_SUCCEEDED,
  result
})

export const QUERY_SAVE_UNSUCCEDDED = 'QUERY_SAVE_UNSUCCEDDED'
export const querySaveUnsucceeded = () => ({ type: QUERY_SAVE_UNSUCCEDDED })

export const QUERY_NEW_REQUESTED = 'QUERY_NEW_REQUESTED'
export const queryNewRequested = () => ({ type: QUERY_NEW_REQUESTED })

export const QUERY_FETCH_SUCCEEDED = 'QUERY_FETCH_SUCCEEDED'
export const receiveQuery = result => ({
  type: QUERY_FETCH_SUCCEEDED,
  result
})

export const QUERY_FETCH_REQUESTED = 'QUERY_FETCH_REQUESTED'
export const queryRequestedFetch = id => ({ type: QUERY_FETCH_REQUESTED, id })

export const QUERY_SEND_BY_EMAIL = 'QUERY_SEND_BY_EMAIL'
export const querySendByEmail = id => ({ type: QUERY_SEND_BY_EMAIL, id })
