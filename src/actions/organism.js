export const ORGANISM_SAVE_REQUESTED = 'ORGANISM_SAVE_REQUESTED'
export function requestSaveOrganism(
  name,
  cuit,
  estimatedDate,
  tel,
  email,
  address,
  idLocation,
  nameLocation,
  idProvince,
  postalCode,
  inaesCode,
  idBank,
  nameBank,
  numberBank,
  isRetired,
  verifyDniLastNumber,
  dniLastNumber,
  actionType
) {
  return {
    type: ORGANISM_SAVE_REQUESTED,
    name,
    cuit,
    estimatedDate,
    tel,
    email,
    address,
    idLocation,
    nameLocation,
    idProvince,
    postalCode,
    inaesCode,
    idBank,
    nameBank,
    numberBank,
    isRetired,
    verifyDniLastNumber,
    dniLastNumber,
    actionType
  }
}

export const ORGANISM_SAVE_SUCCEEDED = 'ORGANISM_SAVE_SUCCEEDED'
export const organismSaveSucceeded = () => ({ type: ORGANISM_SAVE_SUCCEEDED })

export const ORGANISMS_FETCH_REQUESTED = 'ORGANISMS_FETCH_REQUESTED'
export const organismRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: ORGANISMS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const ORGANISMS_FETCH_SUCCEEDED = 'ORGANISMS_FETCH_SUCCEEDED'
export const receiveOrganisms = (result, size, total) => ({
  type: ORGANISMS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const ORGANISM_DEACTIVATE_REQUESTED = 'ORGANISM_DEACTIVATE_REQUESTED'
export const organismRequestedDeactivate = (id, active) => ({
  type: ORGANISM_DEACTIVATE_REQUESTED,
  id,
  active
})

export const ORGANISM_UPDATE_REQUESTED = 'ORGANISM_UPDATE_REQUESTED'
export const organismRequestedUpdate = id => ({
  type: ORGANISM_UPDATE_REQUESTED,
  id
})

export const ORGANISM_FETCH_SUCCEEDED = 'ORGANISM_FETCH_SUCCEEDED'
export const receiveOrganism = result => ({
  type: ORGANISM_FETCH_SUCCEEDED,
  result
})

export const ORGANISMS_ACTIVE_FETCH_REQUESTED =
  'ORGANISMS_ACTIVE_FETCH_REQUESTED'
export const organismsRequestedFetch = (fvalue, fname) => ({
  type: ORGANISMS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_ORGANISM_FINDED = 'RECEIVE_ORGANISM_FINDED'
export const receiveOrganismFinded = result => ({
  type: RECEIVE_ORGANISM_FINDED,
  result
})
