export const REQUEST_VERIFY_TOKEN = 'REQUEST_VERIFY_TOKEN'
export function requestVerifyToken(token) {
  return { type: REQUEST_VERIFY_TOKEN, token }
}

export const LOCKED_USER = 'LOCKED_USER'
export function receiveLockedUser() {
  return { type: LOCKED_USER }
}
export const REQUEST_UPDATE_PASS = 'REQUEST_UPDATE_PASS'
export function requestUpdatePassword(token, user, password, role) {
  return {
    type: REQUEST_UPDATE_PASS,
    token,
    user,
    password,
    role
  }
}

export const REQUESTED_UPDATE_PASS_SUCCEEDED =
  'REQUESTED_UPDATE_PASS_SUCCEEDED '
export function requestUpdatePasswordSucceeded(succeededMsj) {
  return {
    type: REQUESTED_UPDATE_PASS_SUCCEEDED,
    succeededMsj
  }
}
export const VALID_RECOVER_PASS_TOKEN = 'VALID_RECOVER_PASS_TOKEN'
export function receiveVerifyToken(token, user, role) {
  return {
    type: VALID_RECOVER_PASS_TOKEN,
    token,
    user,
    role
  }
}
export const REQUEST_RECOVER_PASS = 'REQUEST_RECOVER_PASS'
export function requestRecoverPass(user, email) {
  return { type: REQUEST_RECOVER_PASS, user, email }
}
export const REQUEST_RECOVER_PASS_SUCCEEDED = 'REQUEST_RECOVER_PASS_SUCCEEDED'
export const requestRecoverPassSucceeded = succeededMsj => ({
  type: REQUEST_RECOVER_PASS_SUCCEEDED,
  succeededMsj
})

export const REQUESTED_FORGET_PASS = 'REQUESTED_FORGET_PASS'
export function requestedForgetPass(user) {
  return { type: REQUESTED_FORGET_PASS, user }
}
export const FETCH_LOGIN_REQUESTED = 'FETCH_LOGIN_REQUESTED'
export function requestLogin(user, password) {
  return { type: FETCH_LOGIN_REQUESTED, user, password }
}

export const FETCH_LOGIN_SUCCEEDED = 'FETCH_LOGIN_SUCCEEDED'
export function requestLoginSucceeded(user, email) {
  return { type: FETCH_LOGIN_SUCCEEDED, user, email }
}
