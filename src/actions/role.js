export const ROLE_FETCH_REQUESTED = 'ROLE_FETCH_REQUESTED'
export const roleRequestedFetch = (fvalue, fname, toUpdate) => ({
  type: ROLE_FETCH_REQUESTED,
  fvalue,
  fname,
  toUpdate
})
export const ROLES_FETCH_REQUESTED = 'ROLES_FETCH_REQUESTED'
export const rolesRequestedFetch = (fvalue, fname) => ({
  type: ROLES_FETCH_REQUESTED,
  fvalue,
  fname
})

export const ROLES_FETCH_SUCCEEDED = 'ROLES_FETCH_SUCCEEDED'
export const receiveRoles = (result, size, total) => ({
  type: ROLES_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const ROLE_FETCH_SUCCEEDED = 'ROLE_FETCH_SUCCEEDED'
export const receiveRole = result => ({
  type: ROLE_FETCH_SUCCEEDED,
  result
})

export const ROLE_FETCH_TO_UPDATE_SUCCEEDED = 'ROLE_FETCH_TO_UPDATE_SUCCEEDED'
export const receiveRoleToUpdate = result => ({
  type: ROLE_FETCH_TO_UPDATE_SUCCEEDED,
  result
})

export const ROLE_SAVE_REQUESTED = 'ROLE_SAVE_REQUESTED'
export function requestSaveRole(jsonAdd, actionType) {
  return {
    type: ROLE_SAVE_REQUESTED,
    jsonAdd,
    actionType
  }
}

export const ROLE_SAVE_SUCCEEDED = 'ROLE_SAVE_SUCCEEDED'
export const roleSaveSucceeded = () => ({ type: ROLE_SAVE_SUCCEEDED })

export const ROLE_UPDATE_REQUESTED = 'ROLE_UPDATE_REQUESTED'
export const roleRequestedUpdate = (fvalue, fname, toUpdate) => ({
  type: ROLE_UPDATE_REQUESTED,
  fvalue,
  fname,
  toUpdate
})

export const ROLE_DEACTIVATE_REQUESTED = 'ROLE_DEACTIVATE_REQUESTED'
export const roleRequestedDeactivate = jsonDeactivate => ({
  type: ROLE_DEACTIVATE_REQUESTED,
  jsonDeactivate
})
