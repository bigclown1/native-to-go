export const LOCATIONS_ACTIVE_FETCH_REQUESTED =
  'LOCATIONS_ACTIVE_FETCH_REQUESTED'
export const locationsRequestedFetch = (fvalue, fname) => ({
  type: LOCATIONS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_LOCATION_FINDED = 'RECEIVE_LOCATION_FINDED'
export const receiveLocationFinded = result => ({
  type: RECEIVE_LOCATION_FINDED,
  result
})
