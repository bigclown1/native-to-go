export const REPORT_QUERIES_SAVE_REQUESTED = 'REPORT_QUERIES_SAVE_REQUESTED'
export function requestSaveReportQueries(jsonAdd, actionType) {
  return { type: REPORT_QUERIES_SAVE_REQUESTED, jsonAdd, actionType }
}

export const REPORT_QUERIES_SAVE_SUCCEEDED = 'REPORT_QUERIES_SAVE_SUCCEEDED'
export const reportQueriesSaveSucceeded = () => ({
  type: REPORT_QUERIES_SAVE_SUCCEEDED
})

export const REPORT_QUERIESS_FETCH_REQUESTED = 'REPORT_QUERIESS_FETCH_REQUESTED'
export const reportQueriesRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: REPORT_QUERIESS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const REPORT_QUERIESS_FETCH_SUCCEEDED = 'REPORT_QUERIESS_FETCH_SUCCEEDED'
export const receiveReportsQueries = (result, size, total) => ({
  type: REPORT_QUERIESS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const REPORT_QUERIES_DEACTIVATE_REQUESTED =
  'REPORT_QUERIES_DEACTIVATE_REQUESTED'
export const reportQueriesRequestedDeactivate = (id, number, active) => ({
  type: REPORT_QUERIES_DEACTIVATE_REQUESTED,
  id,
  number,
  active
})

export const REPORT_QUERIES_UPDATE_REQUESTED = 'REPORT_QUERIES_UPDATE_REQUESTED'
export const reportQueriesRequestedUpdate = id => ({
  type: REPORT_QUERIES_UPDATE_REQUESTED,
  id
})

export const REPORT_QUERIESS_ACTIVE_FETCH_REQUESTED =
  'REPORT_QUERIESS_ACTIVE_FETCH_REQUESTED'
export const reportQueriessRequestedFetch = (fvalue, fname) => ({
  type: REPORT_QUERIESS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname
})

export const RECEIVE_REPORT_QUERIES_FINDED = 'RECEIVE_REPORT_QUERIES_FINDED'
export const receiveReportsQueriesFinded = result => ({
  type: RECEIVE_REPORT_QUERIES_FINDED,
  result
})
export const REPORT_QUERIES_FETCH_SUCCEEDED = 'PLAN_FETCH_SUCCEEDED'
export const receivePlan = result => ({
  type: REPORT_QUERIES_FETCH_SUCCEEDED,
  result
})
