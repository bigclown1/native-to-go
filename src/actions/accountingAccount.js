export const ACCOUNTINGACCOUNT_SAVE_REQUESTED =
  'ACCOUNTINGACCOUNT_SAVE_REQUESTED'
export function requestSaveAccountingAccount(jsonAdd) {
  return {
    type: ACCOUNTINGACCOUNT_SAVE_REQUESTED,
    jsonAdd
  }
}

export const ACCOUNTINGACCOUNT_SAVE_SUCCEEDED =
  'ACCOUNTINGACCOUNT_SAVE_SUCCEEDED'
export const accountingAccountSaveSucceeded = () => ({
  type: ACCOUNTINGACCOUNT_SAVE_SUCCEEDED
})

export const ACCOUNTINGACCOUNTS_FETCH_REQUESTED =
  'ACCOUNTINGACCOUNTS_FETCH_REQUESTED'
export const accountingAccountRequestedFetch = (
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
) => ({
  type: ACCOUNTINGACCOUNTS_FETCH_REQUESTED,
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
})

export const ACCOUNTINGACCOUNTS_FETCH_SUCCEEDED =
  'ACCOUNTINGACCOUNTS_FETCH_SUCCEEDED'
export const receiveAccountingAccounts = (result, size, total) => ({
  type: ACCOUNTINGACCOUNTS_FETCH_SUCCEEDED,
  result,
  size,
  total
})

export const ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED =
  'ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED'
export const accountingAccountRequestedDeactivate = (id, code, active) => ({
  type: ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED,
  id,
  code,
  active
})

export const ACCOUNTINGACCOUNT_UPDATE_REQUESTED =
  'ACCOUNTINGACCOUNT_UPDATE_REQUESTED'
export const accountingAccountRequestedUpdate = id => ({
  type: ACCOUNTINGACCOUNT_UPDATE_REQUESTED,
  id
})

export const ACCOUNTINGACCOUNT_FETCH_SUCCEEDED =
  'ACCOUNTINGACCOUNT_FETCH_SUCCEEDED'
export const receiveAccountingAccount = result => ({
  type: ACCOUNTINGACCOUNT_FETCH_SUCCEEDED,
  result
})

export const ACCOUNTINGACCOUNTS_ACTIVE_FETCH_REQUESTED =
  'ACCOUNTINGACCOUNTS_ACTIVE_FETCH_REQUESTED'
export const accountingAccountsRequestedFetch = (fvalue, fname, withNew) => ({
  type: ACCOUNTINGACCOUNTS_ACTIVE_FETCH_REQUESTED,
  fvalue,
  fname,
  withNew
})

export const RECEIVE_ACCOUNTINGACCOUNT_FINDED =
  'RECEIVE_ACCOUNTINGACCOUNT_FINDED'
export const receiveAccountingAccountFinded = result => ({
  type: RECEIVE_ACCOUNTINGACCOUNT_FINDED,
  result
})
