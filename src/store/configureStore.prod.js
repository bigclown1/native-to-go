import { createStore, applyMiddleware, combineReducers } from 'redux'
import saga from 'redux-saga'
import AsyncStorage from '@react-native-async-storage/async-storage'
import reducers from '../reducers'
import sagas from '../sagas'
import { persistReducer } from 'redux-persist'
const sagaMiddleware = saga()

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['session']
}

export function configureStore(initialState) {
  const store = createStore(
    persistReducer(persistConfig, reducers),
    initialState,
    applyMiddleware(sagaMiddleware)
  )
  sagaMiddleware.run(sagas)

  return store
}
