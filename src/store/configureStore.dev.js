import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import saga from 'redux-saga'

import { composeWithDevTools } from 'remote-redux-devtools'
import reducers from '../reducers'
import sagas from '../sagas'
import AsyncStorage from '@react-native-async-storage/async-storage'
const sagaMiddleware = saga()
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['session']
}

const composeEnhancers = composeWithDevTools({
  name: Platform.OS,
  hostname: 'localhost',
  port: 8000,
  realtime: true
})

export default function configureStore(initialState) {
  const store = createStore(
    persistReducer(persistConfig, reducers),
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  )
  let sagaTask = sagaMiddleware.run(sagas)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(persistReducer(persistConfig, nextRootReducer))
    })
    module.hot.accept('../sagas', () => {
      sagaTask.cancel()
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(sagas)
      })
    })
  }

  return store
}
