import React, { Component } from 'react'
import {
  Container,
  Content,
  Card,
  Text,
  ListItem,
  List,
  Footer,
  FooterTab
} from 'native-base'
import { connect } from 'react-redux'
import {
  flexC,
  flexR,
  linkStyle,
  noPadding,
  small,
  padding
} from '../../utils/styles/index'

import { map } from 'lodash'
class Home extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <Text>Maycoop</Text>
          </Card>
          <Card>
            <Text>Bienvenido {this.props.session.profile.sub}</Text>
          </Card>
          <Card>
            <Text>
              Recuerde que su rol es {this.props.session.profile.ROLE}
            </Text>
          </Card>
          <Card>
            <Text>Los modulos que su rol tiene acceso son:</Text>
            {map(
              map(this.props.session.menuRoutesAll, a => {
                return a.name
              }),
              a => {
                return (
                  <List>
                    <ListItem id={a}>
                      <Text>{a}</Text>
                    </ListItem>
                  </List>
                )
              }
            )}
          </Card>
        </Content>
        <Footer>
          <FooterTab>
            <Text
              styles={{
                ...flexC,
                ...flexR,
                ...linkStyle,
                ...noPadding,
                ...small,
                ...padding
              }}>
              Version Beta 0.0.1
            </Text>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

export default connect(state => ({
  session: state.session
}))(Home)
