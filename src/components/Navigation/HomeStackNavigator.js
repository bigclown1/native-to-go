import React from 'react'
import Home from '../Home/Home'
import { createStackNavigator } from '@react-navigation/stack'

const HomeStack = createStackNavigator()

const HomeStackNavigator = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="HomePage" component={Home} />
    </HomeStack.Navigator>
  )
}

export default HomeStackNavigator
