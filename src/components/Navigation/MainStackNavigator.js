import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Icon from 'react-native-vector-icons/Ionicons'
import AuthStackNavigator from './AuthStackNavigator'
import HomeStackNavigator from './HomeStackNavigator'
import { connect } from 'react-redux'
import { requestVerifyToken } from '../../actions/login'

const RootStack = createStackNavigator()

const MainStackNavigator = ({ session }) => {
  return (
    <NavigationContainer>
      <RootStack.Navigator>
        {!session.token ? (
          <RootStack.Screen
            name="Auth"
            component={AuthStackNavigator}
            options={{
              tabBarIcon: ({ focused, color, size }) => {
                return <Icon name={'ios-home'} size={25} color={color} />
              }
            }}
          />
        ) : (
          <RootStack.Screen
            name="Home"
            component={HomeStackNavigator}
            options={{
              tabBarIcon: ({ focused, color, size }) => {
                return <Icon name={'ios-settings'} size={25} color={color} />
              }
            }}
          />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  )
}
export default connect(
  state => ({
    session: state.session,
    locked: state.login.locked
  }),
  dispatch => ({
    requestVerifyToken: token => dispatch(requestVerifyToken(token))
  })
)(MainStackNavigator)
