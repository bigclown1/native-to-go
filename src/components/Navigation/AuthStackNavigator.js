import React from 'react'
import Login from '../SignIn/Login'
import NewPassword from '../SignIn/NewPassword'
import RecoverPass from '../SignIn/RecoverPass'
import { createStackNavigator } from '@react-navigation/stack'

const AuthStack = createStackNavigator()

const AuthStackNavigator = () => {
  return (
    <AuthStack.Navigator initialRouteName="Login">
      <AuthStack.Screen
        name="Login"
        component={Login}
        options={{ title: 'Login Screen' }}
      />
      <AuthStack.Screen
        name="New Password"
        component={NewPassword}
        options={{ title: 'NP Screen' }}
      />
      <AuthStack.Screen
        name="Recover Password"
        component={RecoverPass}
        options={{ title: 'RC Screen' }}
      />
    </AuthStack.Navigator>
  )
}
export default AuthStackNavigator
