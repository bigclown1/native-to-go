import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  Container,
  Header,
  Body,
  Content,
  Button,
  Text,
  Spinner,
  Item,
  Input,
  Card,
  CardItem
} from 'native-base'
import { requestUpdatePassword } from '../../actions/login'


class newPassword extends Component {
  static propTypes = {
    requestUpdatePassword: PropTypes.func,
    login: PropTypes.shape({
      token: PropTypes.string,
      user: PropTypes.string,
      role: PropTypes.string
    }).isRequired,
    error: PropTypes.bool,
    loading: PropTypes.bool,
    token: PropTypes.string.isRequired
  }

  static defaultProps = {
    loading: false,
    error: false,
    requestUpdatePassword
  }

  constructor(props) {
    super(props)
    this.state = {
      newPass: '',
      newPassConfirm: '',
      msgErrorSame: ''
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const { newPass, newPassConfirm } = this.state
    this.state.msgErrorSame = ''
    if (newPass === newPassConfirm) {
      this.props.requestUpdatePassword(
        this.props.token,
        this.props.login.user,
        newPass,
        this.props.login.role
      )
    } else {
      this.setState({ msgErrorSame: 'Las contraseñas no coinciden' })
    }
  }

  handleChange(e, id) {
    this.setState({ [id]: e })
  }

  render() {
    const { newPass, newPassConfirm } = this.state
    return (
      <Container>
        <Container>
          <Content>
            <Card>
              <CardItem>
                <Text>Ingresa tu nueva password</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Item>
                    <Input
                      type="password"
                      value={newPass}
                      placeholder="Password"
                      secureTextEntry
                      required
                      onChangeText={e => this.handleChange(e, 'newPass')}
                    />
                  </Item>
                  <Item>
                    <Input
                      type="password"
                      value={newPassConfirm}
                      secureTextEntry
                      placeholder="Password"
                      required
                      onChangeText={e => this.handleChange(e, 'newPassConfirm')}
                    />
                  </Item>
                </Body>
              </CardItem>
              <CardItem>
                <Button onPress={e => this.handleSubmit(e)} color="primary">
                  <Text>Enviar nueva contraseña</Text>
                </Button>
              </CardItem>
            </Card>
          </Content>
        </Container>
        {this.props.loading && (
          <Container>
            <Header />
            <Content>
              <Spinner color="blue" />
            </Content>
          </Container>
        )}
        {
          //TODO:  Fix modal to error alert!
          /* {this.props.error && (
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.')
            }}>
            <Text>Se produjo un error</Text>
          </Modal>
        )}
         {this.state.msgErrorSame && (
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.')
            }}>
            {this.state.msgErrorSame}
          </Modal>
        )} //TODO:  Fix modal
          */
        }
      </Container>
    )
  }
}

export default connect(
  state => ({
    session: state.session,
    login: state.login,
    loading: state.login.loading,
    tempPass: state.login.tempPass
  }),
  dispatch => ({
    requestUpdatePassword: (token, id, newPass, role) =>
      dispatch(requestUpdatePassword(token, id, newPass, role))
  })
)(newPassword)
