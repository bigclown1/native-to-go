import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import {
  Container,
  Header,
  Body,
  Content,
  Button,
  Text,
  Spinner,
  Item,
  Input,
  Card,
  CardItem
} from 'native-base'
import {
  requestedForgetPass,
  requestLogin,
  requestVerifyToken
} from '../../actions/login'

class SignIn extends Component {
  static propTypes = {
    profile: PropTypes.shape({
      usuario: PropTypes.string
    }),
    match: PropTypes.shape({
      params: PropTypes.shape({
        token: PropTypes.node
      })
    }),
    session: PropTypes.shape({
      error: PropTypes.bool
    }),
    login: PropTypes.shape({
      error: PropTypes.bool
    }),
    requestVerifyToken: PropTypes.func,
    locked: PropTypes.bool,
    verifyToken: PropTypes.bool,
    loading: PropTypes.bool,
    requestLogin: PropTypes.func,
    requestedForgetPass: PropTypes.func,
    succeededMsj: PropTypes.string
  }

  static defaultProps = {
    profile: null,
    match: {
      params: {
        token: null
      }
    },
    session: {
      error: false
    },
    succeededMsj: null,
    login: {
      user: '',
      role: ''
    },
    locked: false,
    verifyToken: false,
    loading: true,
    requestLogin,
    requestVerifyToken,
    requestedForgetPass
  }

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      token: ''
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const { username, password } = this.state
    this.props.requestLogin(username, password)
    if (this.props.session.token) {
      this.props.navigation.navigate('Home', { screen: 'HomePage' })
    }
  }

  handleChange(e, id) {
    this.setState({ [id]: e })
  }

  handleForgetPass(e) {
    e.preventDefault()
    const { username } = this.state
    this.props.requestedForgetPass(username)
    this.props.navigation.navigate('Recover Password')
  }

  render() {
    return (
      <Container>
        {!this.props.verifyToken && (
          <Container>
            {!this.props.loading && (
              <Content padder contentContainerStyle={styles.content}>
                <Card>
                  <CardItem header bordered>
                    <Text style={styles.textCenter}>
                      Welcome to Maycoop app
                    </Text>
                  </CardItem>
                  <CardItem bordered>
                    <Body style={styles.body}>
                      <Item inlaneLabel>
                        <Input
                          placeholder="Username"
                          id="username"
                          onChangeText={e => this.handleChange(e, 'username')}
                        />
                      </Item>
                      <Item inlaneLabel last>
                        <Input
                          placeholder="Password"
                          id="password"
                          secureTextEntry={true}
                          onChangeText={e => this.handleChange(e, 'password')}
                        />
                      </Item>
                    </Body>
                  </CardItem>
                  <CardItem bordered>
                    <Button
                      style={styles.button}
                      block
                      primary
                      onPress={e => this.handleSubmit(e)}>
                      <Text>Entrar</Text>
                    </Button>
                  </CardItem>
                  <CardItem>
                    <Button
                      transparent
                      style={styles.button}
                      block
                      onPress={e => this.handleForgetPass(e)}>
                      <Text>Olvide mi clave</Text>
                    </Button>
                  </CardItem>
                </Card>
              </Content>
            )}
          </Container>
        )}
        {this.props.loading && (
          <Container>
            <Header />
            <Content>
              <Spinner color="blue" />
            </Content>
          </Container>
        )}
      </Container>
    )
  }
}
export default connect(
  state => ({
    session: state.session,
    verifyToken: state.login.verifyToken,
    login: state.login,
    succeededMsj: state.login.succeededMsj,
    loading: state.login.loading
  }),
  dispatch => ({
    requestVerifyToken: token => dispatch(requestVerifyToken(token)),
    requestLogin: (user, password) => dispatch(requestLogin(user, password)),
    requestedForgetPass: user => dispatch(requestedForgetPass(user))
  })
)(withNavigation(SignIn))

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center'
  },
  textCenter: {
    textAlign: 'center',
    width: '100%'
  },
  body: {
    paddingVertical: 30
  },
  button: {
    marginLeft: '60%'
  }
})
