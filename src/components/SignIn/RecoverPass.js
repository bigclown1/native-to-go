import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Modal } from 'react-native'
import {
  Container,
  Header,
  Body,
  Content,
  Button,
  Text,
  Spinner,
  Item,
  Input,
  Card,
  CardItem
} from 'native-base'
import { requestRecoverPass } from '../../actions/login'
import { Alert } from 'react-native'

class RecoverPass extends Component {
  static propTypes = {
    error: PropTypes.boolean,
    loading: PropTypes.boolean,
    requestRecoverPass: PropTypes.func
  }

  static defaultProps = {
    error: false,
    loading: false,
    requestRecoverPass
  }

  constructor(props) {
    super(props)
    this.state = {
      username: this.props.username,
      email: ''
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const { username, email } = this.state
    this.props.requestRecoverPass(username, email)
  }

  handleChange(e, id) {
    this.setState({ [id]: e })
  }

  render() {
    const { username, email } = this.state
    return (
      <Container>
        <Container>
          <Content>
            <Card>
              <CardItem>
                <Text>Recuperar Password</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Item>
                    <Input
                      type="text"
                      value={username}
                      placeholder="Usuario"
                      required
                      onChangeText={e => this.handleChange(e, 'username')}
                    />
                  </Item>
                  <Item>
                    <Input
                      type="text"
                      value={email}
                      placeholder="Email"
                      required
                      onChange={e => this.handleChange(e, 'email')}
                    />
                  </Item>
                </Body>
              </CardItem>
              <CardItem>
                <Button onPress={e => this.handleSubmit(e)} color="primary">
                  <Text>Recuperar password</Text>
                </Button>
              </CardItem>
            </Card>
          </Content>
        </Container>
        {this.props.loading && (
          <Container>
            <Header />
            <Content>
              <Spinner color="blue" />
            </Content>
          </Container>
        )}
        {this.props.error && (
          <Modal
            animationType="slide"
            onRequestClose={() => {
              Alert.alert('Modal has been closed.')
            }}>
            <Text>El mail o el usuario es incorrecto</Text>
          </Modal>
        )}
      </Container>
    )
  }
}

export default connect(
  state => ({
    error: state.session.error,
    loading: state.login.loading
  }),
  dispatch => ({
    requestRecoverPass: (user, email) =>
      dispatch(requestRecoverPass(user, email))
  })
)(RecoverPass)
