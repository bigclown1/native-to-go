/* Const TokenService = {
    getAuthHeader: () => `Bareer ${StorageService.getAuthToken()}`
};
*/
const endpoint = 'http://maycoop.ddns.net:8080'
const authendpoint = 'http://maycoop.ddns.net:8080'

export default class Http {
  static async get(url, token) {
    const response = await fetch(`${endpoint}${url}`, {
      method: 'get',
      credentials: 'same-origin',
      mode: 'cors',
      cache: 'no-cache',
      headers: {
        'content-type': 'application/json',
        Authorization: token
      }
    })
    try {
      return response.json()
    } catch (parseError) {
      return {
        error: 'Ups... Algo salio mal comunicate con nuestro centro de ayuda',
        response
      }
    }
  }

  static async post(url, body, token) {
    const response = await fetch(`${authendpoint}${url}`, {
      method: 'post',
      credentials: 'same-origin',
      body: JSON.stringify(body),
      headers: {
        'content-type': 'application/json',
        Authorization: token
      }
    })
    try {
      return response.json()
    } catch (parseError) {
      return {
        error: 'Ups... Algo salio mal comunicate con nuestro centro de ayuda',
        response
      }
    }
  }

  static async put(url, body, token) {
    const response = await fetch(`${endpoint}${url}`, {
      method: 'put',
      credentials: 'same-origin',
      body: JSON.stringify(body),
      headers: {
        'content-type': 'application/json',
        Authorization: token
      }
    })
    try {
      return response.json()
    } catch (parseError) {
      return {
        error: 'Ups... Algo salio mal comunicate con nuestro centro de ayuda',
        response
      }
    }
  }

  static async delete(url, body, token) {
    const response = await fetch(`${endpoint}${url}`, {
      method: 'delete',
      credentials: 'same-origin',
      body: JSON.stringify(body),
      headers: {
        'content-type': 'application/json',
        Authorization: token
      }
    })
    return response.json()
  }

  static async postFile(url, file, token) {
    const data = new FormData()
    data.append('file', file)
    const response = await fetch(`${endpoint}${url}`, {
      method: 'post',
      credentials: 'same-origin',
      body: data,
      headers: {
        Authorization: token
      }
    })
    return response.json()
  }

  static async login(url) {
    const response = await fetch(`${authendpoint}${url}`, {
      method: 'post'
    })
    try {
      return response.json()
    } catch (parseError) {
      return {
        error: 'Ups... Algo salio mal comunicate con nuestro centro de ayuda',
        response
      }
    }
  }
}
