import Http from './http'

const ENDPOINT = '/api/role'

export default class RoleService {
  static fetch(token, fvalue, fname, ftype, active) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveRole(token, jsRole) {
    return Http.post(`${ENDPOINT}/add`, jsRole, token)
  }

  static updateRole(token, jsRole) {
    return Http.post(`${ENDPOINT}/update/${jsRole._id}`, jsRole, token)
  }
}
