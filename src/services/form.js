import Http from './http'

const ENDPOINT = '/api/form'
export default class FormService {
  static fetch(token, fname, fvalue) {
    let parameters = ''
    if (token) {
      if (fname) {
        parameters = `?fname=${fname}`
        if (fvalue) {
          parameters += `&fvalue=${fvalue}`
        }
      }
      return Http.getInFormBe(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }
}
