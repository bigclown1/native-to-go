import Http from './http'

const ENDPOINT = '/api/accountingAccount'

export default class AccountingAccountService {
  static fetch(
    token,
    fvalue,
    fname,
    ftype,
    active,
    current,
    columnOrder,
    orderby
  ) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=code'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveAccountingAccount(token, jsonAdd) {
    return Http.post(`${ENDPOINT}/add`, jsonAdd, token)
  }

  static updateAccountingAccount(token, id, jsAccountingAccount) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsAccountingAccount, token)
  }
}
