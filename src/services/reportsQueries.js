import Http from './http'

const ENDPOINT = '/api/reportQueries'

export default class ReportQueriesService {
  static fetch(
    token,
    fvalue,
    fname,
    ftype,
    stype,
    active,
    current,
    columnOrder,
    orderby
  ) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
        parameters += `&stype=${stype}`
      } else {
        parameters += `?stype=${stype}`
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveReportQueries(token, jsReportQueries) {
    return Http.post(`${ENDPOINT}/add`, jsReportQueries, token)
  }

  static updateReportQueries(token, id, jsReportQueries) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsReportQueries, token)
  }
}
