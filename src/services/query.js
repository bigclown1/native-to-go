import Http from './http'

export default class QueryService {
  static sendByEmail(id, token) {
    return Http.get(`/api/loanInQuiry/send_me_by_email/${id}`, token)
  }

  static deleteFilesQuery(id, token, file) {
    return Http.get(`/api/loanInQuiry/deletefile?id=${id}&fn=${file}`, token)
  }

  static uploadFilesQuery(id, token, file, typeFiles) {
    return Http.postFile(
      `/api/loanInQuiry/uploadfile/${typeFiles}/${id}`,
      file,
      token
    )
  }

  static fetchQuery(id, token) {
    return Http.get(`/api/loanInQuiry/get/${id}`, token)
  }

  static fetchQuerys(
    idusuar,
    token,
    fvalue,
    fname,
    ftype,
    current,
    columnOrder,
    orderBy
  ) {
    let parameters = ''
    if (token) {
      if (idusuar) {
        parameters = `?idusuar=${idusuar}`
      }
      if (fvalue) {
        if (parameters) {
          parameters += `&fvalue=${fvalue}`
        } else {
          parameters += `?fvalue=${fvalue}`
        }
        if (!fname) {
          parameters += '&fname=ulmode'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderBy) {
        if (parameters) {
          parameters += `&orderby=${orderBy}`
        } else {
          parameters += `?orderby=${orderBy}`
        }
      }
      return Http.get(`/api/loanInQuiry/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveLoanInQuery(token, jsonAdd, step, id) {
    return Http.post(`/api/loanInQuiry/add/${step}/${id}`, jsonAdd, token)
  }

  static saveLoanMessage(token, jsonMessage, id) {
    return Http.post(`/api/loanInQuiry/addmessage/${id}`, jsonMessage, token)
  }
}
