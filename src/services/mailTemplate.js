import Http from './http'

const ENDPOINT = '/api/mailtemplate'

export default class MailTemplateService {
  static fetch(
    token,
    fvalue,
    fname,
    ftype,
    active,
    current,
    columnOrder,
    orderby
  ) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    return {
      result: null,
      size: null,
      total: null
    }
  }

  static saveMailTemplate(token, jsMailTemplate) {
    return Http.post(`${ENDPOINT}/add`, jsMailTemplate, token)
  }

  static updateMailTemplate(token, id, jsMailTemplate) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsMailTemplate, token)
  }
}
