import decode from 'jwt-decode'
import Http from './http'

const ENDPOINT = '/api/usuar'

export default class UserService {
  static validate(email, username) {
    return Http.get(`${ENDPOINT}/validate/${email}/${username}`)
  }

  static login(username, password) {
    console.log('tr2')
    console.log(username, password)
    return Http.login(`${ENDPOINT}/login/${username}/${password}`)
  }

  static decodetoken(token) {
    if (token) {
      const currentUser = decode(token)
      return currentUser
    }
    return null
  }

  static fetch(token, fvalue, fname, ftype, current, columnOrder, orderby) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }

        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static session() {
    return Http.get(`${ENDPOINT}/session`)
  }

  static saveUser(token, name, pass, email, role) {
    const user = {
      name,
      pass,
      email,
      role
    }
    return Http.post(`${ENDPOINT}/add`, user, token)
  }

  static updateUser(token, id, jsUser) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsUser, token)
  }

  static recoverPass(user, jsUser) {
    return Http.post(`${ENDPOINT}/login/recoverpass/${user}`, jsUser)
  }

  static forgetPass(user) {
    return Http.post(`${ENDPOINT}/login/forgetPass/${user}`)
  }
}
