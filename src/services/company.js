import Http from './http'

const ENDPOINT = '/api/company'

export default class CompanyService {
  static fetch(token, fvalue, fname, ftype, current, columnOrder, orderby) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }

        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveCompany(token, jsCompany) {
    return Http.post(`${ENDPOINT}/add`, jsCompany, token)
  }

  static updateCompany(token, id, jsCompany) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsCompany, token)
  }
}
