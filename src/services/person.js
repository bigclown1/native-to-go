import Http from './http'

const ENDPOINT = '/api/person'

export default class PersonService {
  static fetchNosis(token, dni) {
    let parameters = ''
    if (token) {
      parameters = dni
      return Http.get(`${ENDPOINT}/getnosis/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static fetch(token, fvalue, fname, ftype, active) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static savePerson(token, jsPerson) {
    return Http.post(`${ENDPOINT}/add`, jsPerson, token)
  }

  static updatePerson(token, jsPerson) {
    return Http.post(`${ENDPOINT}/update/${jsPerson.id}`, jsPerson, token)
  }
}
