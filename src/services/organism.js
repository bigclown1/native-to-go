import Http from './http'

const ENDPOINT = '/api/organism'

export default class OrganismService {
  // todo DESTERRAR STYPE
  static fetch(
    token,
    fvalue,
    fname,
    ftype,
    stype,
    current,
    columnOrder,
    orderby
  ) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=name'
        } else {
          parameters += `&fname=${fname}`
        }

        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
        parameters += `&stype=${stype}`
      } else {
        parameters += `?stype=${stype}`
      }
      if (current) {
        if (parameters) {
          parameters += `&start=${current}`
        } else {
          parameters += `?start=${current}`
        }
      }
      if (columnOrder) {
        if (parameters) {
          parameters += `&infoOrder=${columnOrder}`
        } else {
          parameters += `?infoOrder=${columnOrder}`
        }
      }
      if (orderby) {
        if (parameters) {
          parameters += `&orderby=${orderby}`
        } else {
          parameters += `?orderby=${orderby}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static getByCuit(token, cuit, dni) {
    return Http.get(`${ENDPOINT}/getByCuit/${cuit}/${dni}`, token)
  }

  static saveOrganism(token, jsOrganism) {
    return Http.post(`${ENDPOINT}/add`, jsOrganism, token)
  }

  static updateOrganism(token, id, jsOrganism) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsOrganism, token)
  }

  static changueStatusOrganism(token, id, jsOrganism) {
    return Http.post(`${ENDPOINT}/changuestatus/${id}`, jsOrganism, token)
  }
}
