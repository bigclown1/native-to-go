import Http from './http'

const ENDPOINT = '/api/config'

export default class ConfigQueryService {
  static fetch(token, fvalue, fname, ftype, active) {
    let parameters = ''
    if (token) {
      if (fvalue) {
        parameters = `?fvalue=${fvalue}`
        if (!fname) {
          parameters += '&fname=id'
        } else {
          parameters += `&fname=${fname}`
        }
        if (!ftype) {
          parameters += '&ftype=like'
        } else {
          parameters += `&ftype=${ftype}`
        }
      }
      if (active) {
        if (parameters) {
          parameters += `&active=${active}`
        } else {
          parameters += `?active=${active}`
        }
      }
      return Http.get(`${ENDPOINT}/${parameters}`, token)
    }
    const sendNull = {
      result: null,
      size: null,
      total: null
    }
    return sendNull
  }

  static saveConfigQuery(token, id, value) {
    const config = {
      id,
      value
    }
    return Http.post(`${ENDPOINT}/add`, config, token)
  }

  static updateConfigQuery(token, id, jsConfigQuery) {
    return Http.post(`${ENDPOINT}/update/${id}`, jsConfigQuery, token)
  }
}
