import {
  REPORT_QUERIESS_FETCH_SUCCEEDED,
  REPORT_QUERIESS_FETCH_REQUESTED,
  REPORT_QUERIES_DEACTIVATE_REQUESTED,
  REPORT_QUERIES_UPDATE_REQUESTED,
  REPORT_QUERIES_FETCH_SUCCEEDED
} from '../actions/reportsQueries'
import { AN_ERROR_OCCURRED } from '../actions'

export default function reportsQueries(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case REPORT_QUERIES_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case REPORT_QUERIES_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case REPORT_QUERIES_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case REPORT_QUERIESS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case REPORT_QUERIESS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
