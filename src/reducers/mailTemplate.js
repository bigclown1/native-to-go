import {
  MAILTEMPLATE_FETCH_SUCCEEDED,
  MAILTEMPLATES_FETCH_REQUESTED,
  MAILTEMPLATE_DEACTIVATE_REQUESTED,
  MAILTEMPLATE_UPDATE_REQUESTED,
  MAILTEMPLATES_FETCH_SUCCEEDED
} from '../actions/mailTemplate'
import { AN_ERROR_OCCURRED } from '../actions'

export default function mailTemplate(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case MAILTEMPLATE_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case MAILTEMPLATE_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case MAILTEMPLATE_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case MAILTEMPLATES_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case MAILTEMPLATES_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
