import {
  LOANS_FETCH_REQUESTED,
  LOANS_FETCH_SUCCEEDED,
  LOANS_FETCHASKED_REQUESTED
} from '../actions/loanCalculation'
import { AN_ERROR_OCCURRED } from '../actions'
import { QUERY_NEW_REQUESTED } from '../actions/query'

export default function loan(
  state = {
    loading: false,
    result: null,
    size: 0,
    total: 0
  },
  action
) {
  switch (action.type) {
    case QUERY_NEW_REQUESTED:
      return {
        result: null,
        loading: false,
        saved: false
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case LOANS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true
      }
    case LOANS_FETCHASKED_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true
      }
    case LOANS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        size: action.size,
        total: action.total,
        loading: false
      }
    default:
      return state
  }
}
