import {
  CONFIGQUERY_SAVE_REQUESTED,
  CONFIGQUERY_SAVE_SUCCEEDED,
  CONFIGQUERY_FETCH_REQUESTED,
  CONFIGQUERY_FETCH_SUCCEEDED
} from '../actions/configQuery'
import { AN_ERROR_OCCURRED } from '../actions'

export default function configQuery(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case CONFIGQUERY_SAVE_SUCCEEDED:
      return {
        ...state,
        loading: false,
        result: action.result,
        total: 1,
        size: 1
      }
    case CONFIGQUERY_SAVE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case CONFIGQUERY_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case CONFIGQUERY_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        result: action.result,
        total: 1,
        size: 1
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
