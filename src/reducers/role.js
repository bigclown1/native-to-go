import {
  ROLES_FETCH_REQUESTED,
  ROLE_FETCH_SUCCEEDED,
  ROLES_FETCH_SUCCEEDED,
  ROLE_SAVE_SUCCEEDED,
  ROLE_FETCH_REQUESTED,
  ROLE_FETCH_TO_UPDATE_SUCCEEDED
} from '../actions/role'
import { AN_ERROR_OCCURRED } from '../actions'
import { QUERY_NEW_REQUESTED } from '../actions/query'

export default function role(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null,
    update: false
  },
  action
) {
  switch (action.type) {
    case ROLE_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case ROLE_SAVE_SUCCEEDED:
      return {
        ...state,
        update: false
      }
    case ROLE_FETCH_TO_UPDATE_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        update: true,
        total: 1,
        size: 1
      }
    case ROLE_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        updateQuery: true,
        total: 1,
        size: 1
      }
    case QUERY_NEW_REQUESTED:
      return {
        result: null,
        loading: false,
        saved: false
      }
    case ROLES_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case ROLES_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        result: action.result,
        update: false,
        total: 1,
        size: 1
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        result: null,
        loading: false,
        update: false
      }
    default:
      return state
  }
}
