import {
  COMPANIES_FETCH_SUCCEEDED,
  COMPANIES_FETCH_REQUESTED,
  COMPANY_DEACTIVATE_REQUESTED,
  COMPANY_UPDATE_REQUESTED,
  COMPANY_FETCH_SUCCEEDED
} from '../actions/company'
import { AN_ERROR_OCCURRED } from '../actions'

export default function company(
  state = {
    loading: false,
    result: null,
    size: 0,
    total: 0
  },
  action
) {
  switch (action.type) {
    case COMPANY_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case COMPANY_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case COMPANY_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case COMPANIES_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case COMPANIES_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
