import {
  QUERIES_FETCH_REQUESTED,
  QUERIES_FETCH_SUCCEEDED
} from '../actions/queriesStatus'
import { QUERY_FETCH_REQUESTED, QUERY_NEW_REQUESTED } from '../actions/query'
import { AN_ERROR_OCCURRED } from '../actions'

export default function query(
  state = {
    isShowing: false,
    loading: false,
    saved: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case QUERY_NEW_REQUESTED:
      return {
        ...state,
        isShowing: false,
        loading: false,
        saved: false
      }
    case QUERY_FETCH_REQUESTED:
      return {
        ...state,
        isShowing: true,
        loading: false,
        saved: false
      }
    case QUERIES_FETCH_REQUESTED:
      return {
        ...state,
        isShowing: false,
        loading: true,
        saved: false
      }
    case QUERIES_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        saved: false,
        isShowing: false,
        size: action.size,
        total: action.total,
        result: action.result
      }
    default:
      return state
  }
}
