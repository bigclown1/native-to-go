import {
  RECEIVE_LOCATION_FINDED,
  LOCATIONS_ACTIVE_FETCH_REQUESTED
} from '../actions/location'

export default function location(
  state = {
    result: [
      {
        label: 'Crear una nueva',
        value: 'new'
      }
    ],
    isSearching: false
  },
  action
) {
  switch (action.type) {
    case LOCATIONS_ACTIVE_FETCH_REQUESTED:
      return {
        ...state,
        isSearching: true
      }
    case RECEIVE_LOCATION_FINDED:
      return {
        ...state,
        result: action.result,
        isSearching: false
      }
    default:
      return state
  }
}
