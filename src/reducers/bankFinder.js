import { RECEIVE_BANK_FINDED } from '../actions/bank'

export default function bankFinder(
  state = {
    result: [
      {
        label: 'Crear uno nuevo',
        value: 'new'
      }
    ]
  },
  action
) {
  switch (action.type) {
    case RECEIVE_BANK_FINDED:
      return {
        ...state,
        result: action.result
      }
    default:
      return state
  }
}
