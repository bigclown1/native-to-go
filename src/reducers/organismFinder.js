import { RECEIVE_ORGANISM_FINDED } from '../actions/organism'
import { AN_ERROR_OCCURRED } from '../actions'

export default function organismFinder(
  state = {
    result: [
      {
        label: 'Sin asignar',
        value: ''
      }
    ]
  },
  action
) {
  switch (action.type) {
    case RECEIVE_ORGANISM_FINDED:
      return {
        ...state,
        result: action.result
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        result: [
          {
            label: 'Sin asignar',
            value: ''
          }
        ]
      }
    default:
      return state
  }
}
