import { combineReducers } from 'redux'

import abmStatus from './abmStatus'
import error from './error'
import modal from './modal'
import session from './session'
import user from './user'
import bank from './bank'
import bankFinder from './bankFinder'
import organism from './organism'
import organismFinder from './organismFinder'
import location from './location'
import province from './province'
import company from './company'
import companyFinder from './companyFinder'
import person from './person'
import personNosis from './personNosis'
import query from './query'
import queriesStatus from './queriesStatus'
import accountingAccount from './accountingAccount'
import plan from './plan'
import configQuery from './configQuery'
import loan from './loan'
import mailTemplate from './mailTemplate'
import login from './login'
import userFinder from './userFinder'
import reportsQueries from './reportsQueries'
import role from './role'
import form from './form'

export default combineReducers({
  abmStatus,
  error,
  modal,
  session,
  user,
  bank,
  bankFinder,
  organism,
  organismFinder,
  location,
  province,
  company,
  companyFinder,
  person,
  personNosis,
  query,
  queriesStatus,
  plan,
  accountingAccount,
  configQuery,
  loan,
  mailTemplate,
  login,
  userFinder,
  reportsQueries,
  role,
  form
})
