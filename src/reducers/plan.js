import {
  PLANS_FETCH_SUCCEEDED,
  PLANS_FETCH_REQUESTED,
  PLAN_DEACTIVATE_REQUESTED,
  PLAN_UPDATE_REQUESTED,
  PLAN_FETCH_SUCCEEDED
} from '../actions/plan'
import { AN_ERROR_OCCURRED } from '../actions'

export default function plan(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case PLAN_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case PLAN_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case PLAN_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case PLANS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case PLANS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
