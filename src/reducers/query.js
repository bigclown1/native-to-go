import {
  QUERY_NEW_REQUESTED,
  QUERY_SAVE_REQUESTED,
  QUERY_SAVE_SUCCEEDED,
  QUERY_SAVE_UNSUCCEDDED,
  QUERY_FETCH_SUCCEEDED
} from '../actions/query'
import { AN_ERROR_OCCURRED } from '../actions'

export default function query(
  state = {
    isShowing: false,
    loading: false,
    saved: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case QUERY_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        saved: true,
        isShowing: true,
        size: action.size,
        total: action.total,
        result: action.result
      }
    case QUERY_NEW_REQUESTED:
      return {
        result: null,
        isShowing: false,
        loading: false,
        saved: false
      }
    case QUERY_SAVE_REQUESTED:
      return {
        ...state,
        isShowing: false,
        loading: true,
        saved: false
      }
    case QUERY_SAVE_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        isShowing: true,
        loading: false,
        saved: true
      }
    case QUERY_SAVE_UNSUCCEDDED:
      return {
        ...state,
        loading: false,
        saved: false
      }
    default:
      return state
  }
}
