import {
  ORGANISMS_FETCH_SUCCEEDED,
  ORGANISMS_FETCH_REQUESTED,
  ORGANISM_DEACTIVATE_REQUESTED,
  ORGANISM_UPDATE_REQUESTED,
  ORGANISM_FETCH_SUCCEEDED
} from '../actions/organism'
import { AN_ERROR_OCCURRED } from '../actions'

export default function bank(
  state = {
    loading: false,
    result: null,
    size: 0,
    total: 0
  },
  action
) {
  switch (action.type) {
    case ORGANISM_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case ORGANISM_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case ORGANISM_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case ORGANISMS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case ORGANISMS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
