import {
  USERS_FETCH_SUCCEEDED,
  USERS_FETCH_REQUESTED,
  USER_DEACTIVATE_REQUESTED,
  USER_FETCH_SUCCEEDED,
  USER_UPDATE_REQUESTED
} from '../actions/user'
import { AN_ERROR_OCCURRED } from '../actions'

export default function user(
  state = {
    loading: false,
    result: null,
    size: 0,
    total: 0
  },
  action
) {
  switch (action.type) {
    case USER_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case USER_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case USER_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case USERS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case USERS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
