import {
  ACCOUNTINGACCOUNTS_FETCH_SUCCEEDED,
  ACCOUNTINGACCOUNTS_FETCH_REQUESTED,
  ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED,
  ACCOUNTINGACCOUNT_UPDATE_REQUESTED,
  ACCOUNTINGACCOUNT_FETCH_SUCCEEDED
} from '../actions/accountingAccount'
import { AN_ERROR_OCCURRED } from '../actions'

export default function accountingAccount(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case ACCOUNTINGACCOUNT_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case ACCOUNTINGACCOUNT_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case ACCOUNTINGACCOUNTS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case ACCOUNTINGACCOUNTS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
