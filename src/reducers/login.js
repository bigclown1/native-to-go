import {
  FETCH_LOGIN_REQUESTED,
  VALID_RECOVER_PASS_TOKEN,
  REQUEST_UPDATE_PASS,
  LOCKED_USER,
  FETCH_LOGIN_SUCCEEDED,
  REQUESTED_UPDATE_PASS_SUCCEEDED,
  REQUEST_RECOVER_PASS_SUCCEEDED,
  REQUEST_RECOVER_PASS,
  REQUEST_VERIFY_TOKEN
} from '../actions/login'
import { AN_ERROR_OCCURRED } from '../actions'

export default function login(
  state = { loading: false, requested: false, isMobile: false },
  action
) {
  switch (action.type) {
    case VALID_RECOVER_PASS_TOKEN:
      return {
        ...state,
        token: action.token,
        loading: false,
        user: action.user,
        role: action.role,
        verifyToken: true
      }
    case REQUEST_VERIFY_TOKEN:
      return {
        ...state,
        loading: true,
        requested: true,
        error: false
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false,
        requested: false,
        error: true
      }
    case REQUEST_UPDATE_PASS:
      return {
        ...state,
        loading: false,
        requested: true,
        error: false
      }

    case REQUESTED_UPDATE_PASS_SUCCEEDED:
      return {
        ...state,
        loading: false,
        requested: false,
        error: false,
        verifyToken: false,
        succeededMsj: action.succeededMsj
      }
    case FETCH_LOGIN_REQUESTED:
      return {
        ...state,
        loading: true,
        profile: null,
        requested: true,
        error: false
      }
    case REQUEST_RECOVER_PASS:
      return {
        ...state,
        loading: true
      }
    case REQUEST_RECOVER_PASS_SUCCEEDED:
      return {
        ...state,
        loading: false,
        locked: false,
        error: false,
        succeededMsj: action.succeededMsj
      }
    case LOCKED_USER:
      return {
        ...state,
        loading: false,
        locked: true,
        error: false
      }
    case FETCH_LOGIN_SUCCEEDED:
      return {
        ...state,
        loading: false,
        requested: true,
        locked: false
      }
    default:
      return state
  }
}
