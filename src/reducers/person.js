import {
  PERSONS_FETCH_REQUESTED,
  PERSON_FETCH_SUCCEEDED,
  PERSONS_FETCH_SUCCEEDED,
  PERSONS_NOSIS_FETCH_SUCCEEDED,
  PERSON_SAVE_SUCCEEDED,
  PERSON_FETCH_REQUESTED,
  PERSON_FETCH_TO_UPDATE_SUCCEEDED
} from '../actions/person'
import { AN_ERROR_OCCURRED } from '../actions'
import { QUERY_NEW_REQUESTED } from '../actions/query'

export default function person(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null,
    update: false
  },
  action
) {
  switch (action.type) {
    case PERSON_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case PERSON_SAVE_SUCCEEDED:
      return {
        ...state,
        update: false
      }
    case PERSON_FETCH_TO_UPDATE_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        update: true,
        total: 1,
        size: 1
      }
    case PERSON_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        updateQuery: true,
        total: 1,
        size: 1
      }
    case QUERY_NEW_REQUESTED:
      return {
        result: null,
        loading: false,
        saved: false
      }
    case PERSONS_NOSIS_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        update: false
      }
    case PERSONS_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case PERSONS_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        result: action.result,
        update: false,
        total: 1,
        size: 1
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        result: null,
        loading: false,
        update: false
      }
    default:
      return state
  }
}
