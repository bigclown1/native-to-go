import {
  BANKS_FETCH_SUCCEEDED,
  BANKS_FETCH_REQUESTED,
  BANK_DEACTIVATE_REQUESTED,
  BANK_UPDATE_REQUESTED,
  BANK_FETCH_SUCCEEDED
} from '../actions/bank'
import { AN_ERROR_OCCURRED } from '../actions'

export default function bank(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case BANK_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1,
        loading: false
      }
    case BANK_UPDATE_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    case BANK_DEACTIVATE_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case BANKS_FETCH_REQUESTED:
      return {
        ...state,
        result: null,
        loading: true,
        size: null,
        total: null,
        update: false
      }
    case BANKS_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        loading: false,
        size: action.size,
        total: action.total
      }
    default:
      return state
  }
}
