import {
  PERSONS_FETCH_SUCCEEDED,
  PERSONS_NOSIS_FETCH_REQUESTED,
  PERSONS_NOSIS_FETCH_SUCCEEDED
} from '../actions/person'
import { QUERY_NEW_REQUESTED } from '../actions/query'
import { AN_ERROR_OCCURRED } from '../actions'

export default function personNosis(
  state = {
    loading: false,
    size: 0,
    total: 0,
    result: {
      contenido: {
        datos: null
      },
      organism: {
        result: null
      }
    },
    update: false
  },
  action
) {
  switch (action.type) {
    case QUERY_NEW_REQUESTED:
      return {
        result: {
          contenido: {
            datos: null
          },
          organism: {
            result: null
          }
        },
        saved: false
      }
    case PERSONS_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        update: false
      }
    case PERSONS_NOSIS_FETCH_REQUESTED:
      return {
        ...state,
        loading: true
      }
    case PERSONS_NOSIS_FETCH_SUCCEEDED:
      return {
        ...state,
        loading: false,
        result: action.result,
        update: true,
        total: 1,
        size: 1
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false,
        update: false
      }
    default:
      return state
  }
}
