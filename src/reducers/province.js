import { RECEIVE_PROVINCE_FINDED } from '../actions/province'

export default function province(
  state = {
    result: [
      {
        label: 'Sin asignar',
        value: ''
      }
    ]
  },
  action
) {
  switch (action.type) {
    case RECEIVE_PROVINCE_FINDED:
      return {
        ...state,
        result: action.result
      }
    default:
      return state
  }
}
