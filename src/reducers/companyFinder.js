import { RECEIVE_COMPANIES_FINDED } from '../actions/company'
import { AN_ERROR_OCCURRED } from '../actions'

export default function companyFinder(
  state = {
    result: [
      {
        label: 'Sin asignar',
        value: ''
      }
    ]
  },
  action
) {
  switch (action.type) {
    case RECEIVE_COMPANIES_FINDED:
      return {
        ...state,
        result: action.result
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        result: [
          {
            label: 'Sin asignar',
            value: ''
          }
        ]
      }
    default:
      return state
  }
}
