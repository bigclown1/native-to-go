import { AN_ERROR_OCCURRED, SESSION_RECEIVED, IS_MOBILE } from '../actions'

export default function session(
  state = { loading: false, requested: false, isMobile: false },
  action
) {
  switch (action.type) {
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false,
        requested: false,
        error: true
      }
    case SESSION_RECEIVED:
      return {
        // eslint-disable-next-line max-len
        ...state,
        profile: action.profile,
        token: action.token,
        loading: false,
        menuRoutesAll: action.menuRoutesAll,
        routeAll: action.routeAll
      }
    case IS_MOBILE:
      return { ...state, isMobile: action.isMobile }
    default:
      return state
  }
}
