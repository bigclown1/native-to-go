import { FORM_FETCH_REQUESTED, FORM_FETCH_SUCCEEDED } from '../actions/form'
import { AN_ERROR_OCCURRED } from '../actions'

export default function form(
  state = {
    loading: false,
    update: false,
    size: 0,
    total: 0,
    result: null
  },
  action
) {
  switch (action.type) {
    case FORM_FETCH_SUCCEEDED:
      return {
        ...state,
        result: action.result,
        update: true,
        total: 1,
        size: 1,
        loading: false
      }
    case FORM_FETCH_REQUESTED:
      return {
        ...state,
        loading: true,
        update: false
      }
    case AN_ERROR_OCCURRED:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
