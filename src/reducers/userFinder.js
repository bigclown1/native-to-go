import {
  RECEIVE_USER_FINDED,
  USER_ACTIVE_FETCH_REQUESTED_FIND
} from '../actions/user'

export default function userFinder(
  state = {
    result: [
      {
        label: 'Todo',
        value: 'todo'
      }
    ]
  },
  action
) {
  switch (action.type) {
    case RECEIVE_USER_FINDED:
      return {
        ...state,
        result: action.result,
        isSearching: false
      }
    case USER_ACTIVE_FETCH_REQUESTED_FIND:
      return {
        ...state,
        result: null,
        isSearching: true
      }
    default:
      return state
  }
}
