import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import ProvinceService from '../services/province'
import { receiveProvinceFinded } from '../actions/province'

export default function* fetchProvinceFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      ProvinceService.fetchOnlyActive,
      token,
      fvalue,
      fname
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.name,
          value: option.id
        })
      )
      yield put(receiveProvinceFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
