import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import PersonService from '../services/person'
import OrganismService from '../services/organism'
import BankService from '../services/bank'

import {
  receiveNosisPersons,
  receivePersons,
  personNosisRequestedFetch,
  personSaveSucceeded,
  receivePerson,
  personsRequestedFetch,
  receivePersonToUpdate
} from '../actions/person'
import { addSucceeded, requestAdd } from '../actions/abmStatus'

function convertInteger(valor) {
  return valor ? parseInt(valor, 10) : 0
}

function convertFloat(valor) {
  return valor ? parseFloat(valor, 10) : 0
}

export function* fetchNosisPerson({ fvalue }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      PersonService.fetchNosis,
      token,
      fvalue
    )
    let organism = null
    if (!message) {
      const dni = result.contenido.datos.variables[7].valor
      if (result.contenido.datos.variables[65].valor) {
        organism = yield call(
          OrganismService.getByCuit,
          token,
          result.contenido.datos.variables[65].valor,
          dni
        )
      }
      if (organism && organism.message) {
        organism = null
      }
      result.organism = organism
      const jsBank = {
        isBanked: result.contenido.datos.variables[83].valor,
        numberOfBanks: convertInteger(
          result.contenido.datos.variables[80].valor
        ),
        totalDue: convertFloat(result.contenido.datos.variables[81].valor),
        numberOfBanksInSituation1: convertInteger(
          result.contenido.datos.variables[85].valor
        ),
        amountOfBanksInSituation1: convertFloat(
          result.contenido.datos.variables[86].valor
        ),
        numberOfBanksInSituation2: convertInteger(
          result.contenido.datos.variables[87].valor
        ),
        amountOfBanksInSituation2: convertFloat(
          result.contenido.datos.variables[88].valor
        ),
        numberOfBanksInSituation3: convertInteger(
          result.contenido.datos.variables[89].valor
        ),
        amountOfBanksInSituation3: convertFloat(
          result.contenido.datos.variables[90].valor
        ),
        numberOfBanksInSituation4: convertInteger(
          result.contenido.datos.variables[91].valor
        ),
        amountOfBanksInSituation4: convertFloat(
          result.contenido.datos.variables[92].valor
        ),
        numberOfBanksInSituation5: convertInteger(
          result.contenido.datos.variables[93].valor
        ),
        amountOfBanksInSituation5: convertFloat(
          result.contenido.datos.variables[94].valor
        ),
        numberOfBanksInSituation6: convertInteger(
          result.contenido.datos.variables[95].valor
        ),
        amountOfBanksInSituation6: convertFloat(
          result.contenido.datos.variables[96].valor
        )
      }

      jsBank.qSituationFiveOrGreater =
        jsBank.numberOfBanksInSituation5 + jsBank.numberOfBanksInSituation6

      if (
        !jsBank.numberOfBanksInSituation6 &&
        jsBank.numberOfBanksInSituation6 > 0
      ) {
        result.worstBankSituation = 6
      } else if (
        !jsBank.numberOfBanksInSituation5 &&
        jsBank.numberOfBanksInSituation5 > 0
      ) {
        result.worstBankSituation = 5
      } else if (
        !jsBank.numberOfBanksInSituation4 &&
        jsBank.numberOfBanksInSituation4 > 0
      ) {
        result.worstBankSituation = 4
      } else if (
        !jsBank.numberOfBanksInSituation3 &&
        jsBank.numberOfBanksInSituation3 > 0
      ) {
        result.worstBankSituation = 3
      } else if (
        !jsBank.numberOfBanksInSituation2 &&
        jsBank.numberOfBanksInSituation2 > 0
      ) {
        result.worstBankSituation = 2
      } else {
        result.worstBankSituation = 1
      }

      const banks = []
      let bank = null
      let indexOfResultNosisBank = 97
      while (indexOfResultNosisBank <= 133) {
        if (
          convertInteger(
            result.contenido.datos.variables[indexOfResultNosisBank].valor
          ) !== 0
        ) {
          bank = yield call(
            BankService.fetch,
            token,
            result.contenido.datos.variables[indexOfResultNosisBank].valor,
            'number',
            'equal',
            'S'
          )
        }
        if (bank !== null && !bank.message) {
          banks.push(bank)
        }
        indexOfResultNosisBank += 4
      }

      result.maxNumberCommercial = result.contenido.datos.variables[267].valor

      result.bankInformation = jsBank
      result.bankInformation.banks = banks

      const jsNosis = {
        currentScore: convertInteger(
          result.contenido.datos.variables[274].valor
        ),
        isRetired: result.contenido.datos.variables[50].valor,
        laborOld: convertInteger(result.contenido.datos.variables[62].valor)
      }
      result.nosis = jsNosis

      yield put(receiveNosisPersons(result, 1, 1))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchPerson({ fvalue, fname, toUpdate }) {
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      PersonService.fetch,
      token,
      fvalue,
      fname,
      'like',
      'S'
    )
    if (!message) {
      if (toUpdate) {
        yield put(receivePersonToUpdate(result, size, total))
      } else {
        yield put(receivePerson(result, size, total))
      }
    } else {
      yield put(personNosisRequestedFetch(fvalue))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchPersons({ fvalue, fname }) {
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      PersonService.fetch,
      token,
      fvalue,
      fname,
      'like',
      'S'
    )
    if (!message) {
      yield put(receivePersons(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* savePerson({ jsonAdd, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddPerson
    if (actionType === 'update') {
      const jsonUpdate = jsonAdd
      jsonUpdate.id = yield select(state => state.person.result[0].id)
      serviceAddPerson = yield call(
        PersonService.updatePerson,
        token,
        jsonUpdate
      )
    } else {
      serviceAddPerson = yield call(PersonService.savePerson, token, jsonAdd)
    }
    if (serviceAddPerson.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddPerson.message
        })
      )
    } else {
      yield put(personSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivatePerson({ jsonDeactivate }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsonPerson = jsonDeactivate
    if (jsonPerson.active === 'N') {
      Object.assign(jsonPerson, {
        active: 'S'
      })
    } else {
      Object.assign(jsonPerson, {
        active: 'N'
      })
    }
    const serviceAddPerson = yield call(
      PersonService.updatePerson,
      token,
      jsonPerson
    )
    if (serviceAddPerson.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddPerson.message
        })
      )
    } else {
      yield put(personsRequestedFetch(null, null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
