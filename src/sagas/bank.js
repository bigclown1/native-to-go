import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  bankSaveSucceeded,
  receiveBanks,
  bankRequestedFetch,
  receiveBank,
  receiveBankFinded
} from '../actions/bank'
import BankService from '../services/bank'

export function* fetchBankFinder({ fvalue, fname, withNew }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      BankService.fetch,
      token,
      fvalue,
      fname,
      null,
      'S'
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.name,
          value: option.id
        })
      )
      yield put(receiveBankFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
      if (message === 'No hay bancos') {
        let newBankJs = []
        if (withNew) {
          newBankJs = [
            {
              label: 'Crear uno nuevo',
              value: 'new'
            }
          ]
        } else {
          newBankJs = [
            {
              label: 'Sin Asignar',
              value: ''
            }
          ]
        }
        yield put(receiveBankFinded(newBankJs))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchBank({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      BankService.fetch,
      token,
      id,
      'Id',
      'like',
      'A'
    )
    if (!message) {
      yield put(receiveBank(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchBanks({ fvalue, fname, current, columnOrder, orderby }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      BankService.fetch,
      token,
      fvalue,
      fname,
      null,
      'A',
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveBanks(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

// TODO AleSierra, reemplazar {id, number, active} por {jsonXxxxx}
export function* deactivateBank({ id, number, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsBank = {
      id,
      number
    }
    if (active === 'N') {
      Object.assign(jsBank, {
        active: 'S'
      })
    } else {
      Object.assign(jsBank, {
        active: 'N'
      })
    }
    const serviceUpdateBank = yield call(BankService.updateBank, token, jsBank)
    if (serviceUpdateBank.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateBank.message
        })
      )
    } else {
      yield put(bankRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveBank({ jsonAdd, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddBank
    if (actionType === 'update') {
      const jsBank = jsonAdd
      jsBank.id = yield select(state => state.bank.result[0].id)
      serviceAddBank = yield call(BankService.updateBank, token, jsBank)
    } else {
      serviceAddBank = yield call(BankService.saveBank, token, jsonAdd)
    }
    if (serviceAddBank.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddBank.message
        })
      )
    } else {
      yield put(bankSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
