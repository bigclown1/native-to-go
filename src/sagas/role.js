import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import RoleService from '../services/role'

import {
  receiveRoles,
  roleSaveSucceeded,
  receiveRole,
  rolesRequestedFetch,
  receiveRoleToUpdate
} from '../actions/role'
import { addSucceeded, requestAdd } from '../actions/abmStatus'

export function* fetchRole({ fvalue, fname, toUpdate }) {
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      RoleService.fetch,
      token,
      fvalue,
      fname,
      'like',
      'S'
    )
    if (!message) {
      if (toUpdate) {
        yield put(receiveRoleToUpdate(result, size, total))
      } else {
        yield put(receiveRole(result, size, total))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchRoles({ fvalue, fname }) {
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      RoleService.fetch,
      token,
      fvalue,
      fname,
      'like',
      'S'
    )
    if (!message) {
      yield put(receiveRoles(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveRole({ jsonAdd, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddRole
    if (actionType === 'update') {
      const jsonUpdate = jsonAdd
      jsonUpdate.id = yield select(state => state.role.result[0]._id)
      serviceAddRole = yield call(RoleService.updateRole, token, jsonUpdate)
    } else {
      serviceAddRole = yield call(RoleService.saveRole, token, jsonAdd)
    }
    if (serviceAddRole.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddRole.message
        })
      )
    } else {
      yield put(roleSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateRole({ jsonDeactivate }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsonRole = jsonDeactivate
    if (jsonRole.active === 'N') {
      Object.assign(jsonRole, {
        active: 'S'
      })
    } else {
      Object.assign(jsonRole, {
        active: 'N'
      })
    }
    const serviceAddRole = yield call(RoleService.updateRole, token, jsonRole)
    if (serviceAddRole.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddRole.message
        })
      )
    } else {
      yield put(rolesRequestedFetch(null, null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
