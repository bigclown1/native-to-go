import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  reportQueriesSaveSucceeded,
  reportQueriesRequestedFetch,
  receiveReportsQueries
} from '../actions/reportsQueries'
import ReportQueriesService from '../services/reportsQueries'

export function* fetchReportQueries({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      ReportQueriesService.fetch,
      token,
      id,
      'reportQueries.Id',
      'like',
      'A'
    )
    if (!message) {
      yield put(receiveReportsQueries(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchReportQueriess({
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      ReportQueriesService.fetch,
      token,
      fvalue,
      fname,
      null,
      'R',
      'A',
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveReportsQueries(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateReportQueries({ id, number, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsReportQueries = {
      id,
      number
    }
    if (active === 'N') {
      Object.assign(jsReportQueries, {
        active: 'S'
      })
    } else {
      Object.assign(jsReportQueries, {
        active: 'N'
      })
    }
    const serviceUpdateReportQueries = yield call(
      ReportQueriesService.updateReportQueries,
      token,
      id,
      jsReportQueries
    )
    if (serviceUpdateReportQueries.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateReportQueries.message
        })
      )
    } else {
      yield put(reportQueriesRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveReportQueries({ jsonAdd, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    const idUsuar = yield select(state => state.session.profile.jti)
    let serviceAddReportQueries
    if (actionType === 'update') {
      const id = yield select(state => state.reportsQueries.result[0]._id)
      serviceAddReportQueries = yield call(
        ReportQueriesService.updateReportQueries,
        token,
        id,
        jsonAdd
      )
    } else {
      const js = jsonAdd
      js.user = {
        id: idUsuar
      }
      if (js.userFilter) {
        js.userFilter = {
          id: jsonAdd.userFilter
        }
      }
      serviceAddReportQueries = yield call(
        ReportQueriesService.saveReportQueries,
        token,
        js
      )
    }
    if (serviceAddReportQueries.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddReportQueries.message
        })
      )
    } else {
      yield put(reportQueriesSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
