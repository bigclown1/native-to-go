import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  companySaveSucceeded,
  receiveCompanies,
  receiveCompany,
  companyRequestedFetch,
  receiveCompaniesFinded
} from '../actions/company'
import CompanyService from '../services/company'

export function* fetchCompany({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      CompanyService.fetch,
      token,
      id,
      'Id',
      'like'
    )
    if (!message) {
      yield put(receiveCompany(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchCompanyes({ fvalue, current, columnOrder, orderby }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      CompanyService.fetch,
      token,
      fvalue,
      null,
      null,
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveCompanies(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateCompany({
  id,
  name,
  regimen,
  cuit,
  tel,
  email,
  address,
  location,
  active
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsCompany = {
      id,
      name,
      regimen,
      cuit,
      tel,
      email,
      address,
      location: {
        id: location.id
      }
    }
    if (active === 'N') {
      Object.assign(jsCompany, {
        active: 'S'
      })
    } else {
      Object.assign(jsCompany, {
        active: 'N'
      })
    }
    const serviceUpdateCompany = yield call(
      CompanyService.updateCompany,
      token,
      id,
      jsCompany
    )
    if (serviceUpdateCompany.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateCompany.message
        })
      )
    } else {
      yield put(companyRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveCompany({
  name,
  regimen,
  cuit,
  tel,
  email,
  address,
  idLocation,
  nameLocation,
  idProvince,
  postalCode,
  inaesCode,
  actionType
}) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddCompany
    if (actionType === 'update') {
      const id = yield select(state => state.company.result[0].id)
      const jsCompanyUpdate = {
        id,
        name,
        regimen,
        cuit,
        tel,
        email,
        address,
        active: 'S'
      }
      if (idLocation === 'new') {
        Object.assign(jsCompanyUpdate, {
          location: {
            name: nameLocation,
            province: {
              id: idProvince
            },
            cp: postalCode,
            codInaes: inaesCode
          }
        })
      } else {
        Object.assign(jsCompanyUpdate, {
          location: {
            id: idLocation
          }
        })
      }
      serviceAddCompany = yield call(
        CompanyService.updateCompany,
        token,
        id,
        jsCompanyUpdate
      )
    } else {
      const jsCompanyAdd = {
        name,
        regimen,
        cuit,
        tel,
        email,
        address,
        active: 'S'
      }
      if (idLocation === 'new') {
        Object.assign(jsCompanyAdd, {
          location: {
            name: nameLocation,
            province: {
              id: idProvince
            },
            cp: postalCode,
            codInaes: inaesCode
          }
        })
      } else {
        Object.assign(jsCompanyAdd, {
          location: {
            id: idLocation
          }
        })
      }
      serviceAddCompany = yield call(
        CompanyService.saveCompany,
        token,
        jsCompanyAdd
      )
    }
    if (serviceAddCompany.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddCompany.message
        })
      )
    } else {
      yield put(companySaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchCompaniesFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      CompanyService.fetch,
      token,
      fvalue,
      fname
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.name,
          value: option.id
        })
      )
      yield put(receiveCompaniesFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
