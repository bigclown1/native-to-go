import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import {
  querySaveSucceeded,
  receiveQuery,
  queryNewRequested
} from '../actions/query'
import { receiveQueries } from '../actions/queriesStatus'
import {
  loanAskedRequestedFetch,
  loanRequestedFetch,
  receiveLoan
} from '../actions/loanCalculation'
import QueryService from '../services/query'
import { addSucceeded, requestAdd } from '../actions/abmStatus'
import { personNosisRequestedFetch } from '../actions/person'

export function* sendByEmail({ id }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    const sendByEmailResponse = yield call(QueryService.sendByEmail, id, token)
    yield put(
      anErrorOccurred({
        anErrorOccurred: true,
        errorMsg: sendByEmailResponse.message
      })
    )
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveMessage({ jsonMessage, id }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    const saveLoanMessageResponse = yield call(
      QueryService.saveLoanMessage,
      token,
      jsonMessage,
      id
    )
    if (!saveLoanMessageResponse.message) {
      yield put(querySaveSucceeded(saveLoanMessageResponse.result))
      yield put(addSucceeded())
    } else {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: saveLoanMessageResponse.message
        })
      )
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deleteFileQuery({ id, file }) {
  yield put(clearError())
  yield put(requestAdd())
  try {
    const token = yield select(state => state.session.token)
    const { message, result } = yield call(
      QueryService.deleteFilesQuery,
      id,
      token,
      file
    )
    if (message) {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    } else {
      yield put(querySaveSucceeded(result))
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* uploadFileQuery({ file, typeFiles, id }) {
  yield put(clearError())
  yield put(requestAdd())
  try {
    const token = yield select(state => state.session.token)
    const { message, result } = yield call(
      QueryService.uploadFilesQuery,
      id,
      token,
      file,
      typeFiles
    )
    if (message) {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    } else {
      yield put(querySaveSucceeded(result))
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* newQuery() {
  yield put(clearError())
}

export function* fetchQuery({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { message, result } = yield call(QueryService.fetchQuery, id, token)
    if (!message) {
      yield put(personNosisRequestedFetch(result.person.dni))
      yield put(receiveQuery(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchQuerys({
  idUsuar,
  fvalue,
  fname,
  current,
  columnOrder,
  orderBy
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { message, result, size, total } = yield call(
      QueryService.fetchQuerys,
      idUsuar,
      token,
      fvalue,
      fname,
      null,
      current,
      columnOrder,
      orderBy
    )
    if (!message) {
      yield put(receiveQueries(result, size, total))
    } else {
      yield put(receiveQueries(null, 0, 0))
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveLoanInQuery({ jsonAdd, step, id }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const idUsuar = yield select(state => state.session.profile.jti)
    const token = yield select(state => state.session.token)
    const saveLoanInQueryResponse = yield call(
      QueryService.saveLoanInQuery,
      token,
      jsonAdd,
      step,
      id
    )
    const response = {
      result: saveLoanInQueryResponse.result,
      size: saveLoanInQueryResponse.size,
      total: saveLoanInQueryResponse.total
    }
    if (!saveLoanInQueryResponse.message) {
      if (step === 4) {
        yield put(receiveLoan(response.result, response.size, response.total))
      } else if (step === 6) {
        const { message, result, size, total } = yield call(
          QueryService.fetchQuerys,
          idUsuar,
          token,
          null,
          null,
          null,
          null,
          null,
          null,
          null
        )
        if (!message) {
          yield put(queryNewRequested())
          yield put(receiveQueries(result, size, total))
        } else {
          yield put(
            anErrorOccurred({ anErrorOccurred: true, errorMsg: message })
          )
        }
      } else {
        yield put(querySaveSucceeded(response.result))
      }
      if (step !== 6) {
        yield put(addSucceeded())
      }
    } else {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: saveLoanInQueryResponse.message
        })
      )
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchLoan(loanCalculation) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      QueryService.fetchLoanCalculation,
      token,
      loanCalculation.loanCalculation
    )
    if (!message) {
      yield put(receiveLoan(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchAskedLoan(loanCalculation) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      QueryService.fetchAskedLoanCalculation,
      token,
      loanCalculation.loanCalculation
    )
    if (!message) {
      yield put(receiveLoan(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveQuery({
  fullName,
  dni,
  cuil,
  sex,
  birthDay,
  email,
  nationality,
  maritalStatus,
  idOrganism,
  idBank,
  salary,
  bankFeeCredits,
  cardDebits,
  discovered,
  debits,
  deposits,
  extractions,
  laborOld,
  collectionBankSituation,
  qSituationFiveOrGreater,
  nosisScore,
  amountAsked,
  worstBankSituation,
  maxNumberCommercial
}) {
  yield put(clearError())
  try {
    yield put(
      querySaveSucceeded(
        fullName,
        dni,
        cuil,
        sex,
        birthDay,
        email,
        nationality,
        maritalStatus,
        idOrganism,
        idBank,
        salary,
        bankFeeCredits,
        cardDebits,
        discovered,
        debits,
        deposits,
        extractions,
        laborOld,
        collectionBankSituation,
        qSituationFiveOrGreater,
        nosisScore,
        amountAsked,
        worstBankSituation,
        maxNumberCommercial
      )
    )
    if (
      salary &&
      bankFeeCredits &&
      cardDebits &&
      discovered &&
      debits &&
      deposits &&
      extractions
    ) {
      const loanCalculation = {
        person: {
          fullName,
          dni,
          cuil,
          sex,
          birthDay,
          nationality,
          civilStatus: maritalStatus,
          email,
          organism: {
            id: idOrganism
          }
        },
        laborOld,
        collectionBankSituation,
        qSituationFiveOrGreater,
        nosisScore,
        salary,
        bankFeeCredits,
        cardDebits,
        discovered,
        debits,
        deposits,
        extractions,
        amountAsked,
        worstBankSituation,
        maxNumberCommercial
      }
      if (amountAsked > 0) {
        yield put(loanAskedRequestedFetch(loanCalculation))
      } else {
        yield put(loanRequestedFetch(loanCalculation))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
