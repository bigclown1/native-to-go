import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { receiveFormSucceeded } from '../actions/form'
import FormService from '../services/form'

export default function* fetchForm({ fname, fvalue }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      FormService.fetch,
      token,
      fname,
      fvalue
    )
    if (!message) {
      yield put(receiveFormSucceeded(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err.message }))
  }
}
