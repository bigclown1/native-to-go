import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  accountingAccountSaveSucceeded,
  receiveAccountingAccounts,
  accountingAccountRequestedFetch,
  receiveAccountingAccount,
  receiveAccountingAccountFinded
} from '../actions/accountingAccount'
import AccountingAccountService from '../services/accountingAccount'

export function* fetchAccountingAccountFinder({ fvalue, fname, withNew }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      AccountingAccountService.fetch,
      token,
      fvalue,
      fname,
      null,
      'S'
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.code,
          value: option.id
        })
      )
      yield put(receiveAccountingAccountFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
      if (message === 'No hay cuentas') {
        let newAccountingAccountJs = []
        if (withNew) {
          newAccountingAccountJs = [
            {
              label: 'Crear una nueva',
              value: 'new'
            }
          ]
        } else {
          newAccountingAccountJs = [
            {
              label: 'Sin Asignar',
              value: ''
            }
          ]
        }
        yield put(receiveAccountingAccountFinded(newAccountingAccountJs))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchAccountingAccount({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      AccountingAccountService.fetch,
      token,
      id,
      'accountingAccount.id',
      'like',
      'A'
    )
    if (!message) {
      yield put(receiveAccountingAccount(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchAccountingAccounts({
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      AccountingAccountService.fetch,
      token,
      fvalue,
      fname,
      null,
      'A',
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveAccountingAccounts(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateAccountingAccount({ id, code, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsAccountingAccount = {
      id,
      code
    }
    if (active === 'N') {
      Object.assign(jsAccountingAccount, {
        active: 'S'
      })
    } else {
      Object.assign(jsAccountingAccount, {
        active: 'N'
      })
    }
    const serviceUpdateAccountingAccount = yield call(
      AccountingAccountService.updateAccountingAccount,
      token,
      id,
      jsAccountingAccount
    )
    if (serviceUpdateAccountingAccount.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateAccountingAccount.message
        })
      )
    } else {
      yield put(accountingAccountRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveAccountingAccount({ jsonAdd }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddAccountingAccount
    if (jsonAdd.actionType === 'update') {
      const id = yield select(state => state.accountingAccount.result[0].id)
      const jsAccountingAccount = jsonAdd
      jsAccountingAccount.active = 'S'
      jsAccountingAccount.id = id
      serviceAddAccountingAccount = yield call(
        AccountingAccountService.updateAccountingAccount,
        token,
        id,
        jsAccountingAccount
      )
    } else {
      serviceAddAccountingAccount = yield call(
        AccountingAccountService.saveAccountingAccount,
        token,
        jsonAdd
      )
    }
    if (serviceAddAccountingAccount.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddAccountingAccount.message
        })
      )
    } else {
      yield put(accountingAccountSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
