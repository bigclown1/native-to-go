import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  organismSaveSucceeded,
  receiveOrganisms,
  organismRequestedFetch,
  receiveOrganism,
  receiveOrganismFinded
} from '../actions/organism'
import OrganismService from '../services/organism'

export function* fetchOrganismFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      OrganismService.fetch,
      token,
      fvalue,
      fname,
      null,
      'S'
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.name,
          value: option.id
        })
      )
      yield put(receiveOrganismFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchOrganism({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      OrganismService.fetch,
      token,
      id,
      'Id',
      'like',
      'C'
    )
    if (!message) {
      yield put(receiveOrganism(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchOrganisms({
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      OrganismService.fetch,
      token,
      fvalue,
      fname,
      null,
      'R',
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveOrganisms(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateOrganism({ id, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsOrganism = {
      id
    }
    if (active === 'N') {
      Object.assign(jsOrganism, {
        active: 'S'
      })
    } else {
      Object.assign(jsOrganism, {
        active: 'N'
      })
    }
    const serviceUpdateOrganism = yield call(
      OrganismService.changueStatusOrganism,
      token,
      id,
      jsOrganism
    )
    if (serviceUpdateOrganism.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateOrganism.message
        })
      )
    } else {
      yield put(organismRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveOrganism({
  name,
  cuit,
  estimatedDate,
  tel,
  email,
  address,
  idLocation,
  nameLocation,
  idProvince,
  postalCode,
  inaesCode,
  idBank,
  nameBank,
  numberBank,
  isRetired,
  verifyDniLastNumber,
  dniLastNumber,
  actionType
}) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddOrganism
    if (actionType === 'update') {
      const id = yield select(state => state.organism.result[0].id)
      const jsOrganismUpdate = {
        id,
        name,
        cuit,
        estimatedDate,
        tel,
        email,
        address,
        isRetired,
        verifyDniLastNumber,
        dniLastNumber,
        active: 'S'
      }
      if (idLocation === 'new') {
        Object.assign(jsOrganismUpdate, {
          location: {
            name: nameLocation,
            province: {
              id: idProvince
            },
            cp: postalCode,
            codInaes: inaesCode
          }
        })
      } else {
        Object.assign(jsOrganismUpdate, {
          location: {
            id: idLocation
          }
        })
      }
      if (idBank === 'new') {
        Object.assign(jsOrganismUpdate, {
          bank: {
            name: nameBank,
            number: numberBank
          }
        })
      } else {
        Object.assign(jsOrganismUpdate, {
          bank: {
            id: idBank
          }
        })
      }

      serviceAddOrganism = yield call(
        OrganismService.updateOrganism,
        token,
        id,
        jsOrganismUpdate
      )
    } else {
      const jsOrganismAdd = {
        name,
        cuit,
        estimatedDate,
        tel,
        email,
        address,
        isRetired,
        verifyDniLastNumber,
        dniLastNumber,
        active: 'S'
      }
      if (idLocation === 'new') {
        Object.assign(jsOrganismAdd, {
          location: {
            name: nameLocation,
            province: {
              id: idProvince
            },
            cp: postalCode,
            codInaes: inaesCode
          }
        })
      } else {
        Object.assign(jsOrganismAdd, {
          location: {
            id: idLocation
          }
        })
      }
      if (idBank === 'new') {
        Object.assign(jsOrganismAdd, {
          bank: {
            name: nameBank,
            number: numberBank
          }
        })
      } else {
        Object.assign(jsOrganismAdd, {
          bank: {
            id: idBank
          }
        })
      }
      serviceAddOrganism = yield call(
        OrganismService.saveOrganism,
        token,
        jsOrganismAdd
      )
    }
    if (serviceAddOrganism.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddOrganism.message
        })
      )
    } else {
      yield put(organismSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
