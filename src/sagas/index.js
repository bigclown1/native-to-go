import { takeEvery, all } from 'redux-saga/effects'
import {
  FETCH_LOGIN_REQUESTED,
  REQUEST_RECOVER_PASS,
  REQUEST_UPDATE_PASS,
  REQUESTED_FORGET_PASS,
  REQUEST_VERIFY_TOKEN
} from '../actions/login'
import { FORM_FETCH_REQUESTED } from '../actions/form'
import {
  USER_SAVE_REQUESTED,
  USERS_FETCH_REQUESTED,
  USER_DEACTIVATE_REQUESTED,
  USER_UPDATE_REQUESTED,
  USER_ACTIVE_FETCH_REQUESTED_FIND
} from '../actions/user'
import {
  BANK_SAVE_REQUESTED,
  BANKS_FETCH_REQUESTED,
  BANK_DEACTIVATE_REQUESTED,
  BANK_UPDATE_REQUESTED,
  BANKS_ACTIVE_FETCH_REQUESTED
} from '../actions/bank'
import {
  ORGANISMS_FETCH_REQUESTED,
  ORGANISM_SAVE_REQUESTED,
  ORGANISM_DEACTIVATE_REQUESTED,
  ORGANISM_UPDATE_REQUESTED,
  ORGANISMS_ACTIVE_FETCH_REQUESTED
} from '../actions/organism'
import { LOCATIONS_ACTIVE_FETCH_REQUESTED } from '../actions/location'
import { PROVINCES_ACTIVE_FETCH_REQUESTED } from '../actions/province'
import {
  CONFIGQUERY_FETCH_REQUESTED,
  CONFIGQUERY_SAVE_REQUESTED
} from '../actions/configQuery'
import {
  COMPANIES_ACTIVE_FETCH_REQUESTED,
  COMPANIES_FETCH_REQUESTED,
  COMPANY_DEACTIVATE_REQUESTED,
  COMPANY_SAVE_REQUESTED,
  COMPANY_UPDATE_REQUESTED
} from '../actions/company'
import {
  PERSON_SAVE_REQUESTED,
  PERSONS_FETCH_REQUESTED,
  PERSONS_NOSIS_FETCH_REQUESTED,
  PERSON_UPDATE_REQUESTED,
  PERSON_DEACTIVATE_REQUESTED,
  PERSON_FETCH_REQUESTED
} from '../actions/person'

import {
  ROLE_SAVE_REQUESTED,
  ROLES_FETCH_REQUESTED,
  ROLE_UPDATE_REQUESTED,
  ROLE_DEACTIVATE_REQUESTED
} from '../actions/role'

import {
  QUERY_FETCH_REQUESTED,
  QUERY_SAVE_REQUESTED,
  QUERY_NEW_REQUESTED,
  QUERY_SEND_BY_EMAIL
} from '../actions/query'
import { QUERIES_FETCH_REQUESTED } from '../actions/queriesStatus'
import {
  PLAN_DEACTIVATE_REQUESTED,
  PLAN_SAVE_REQUESTED,
  PLAN_UPDATE_REQUESTED,
  PLANS_FETCH_REQUESTED
} from '../actions/plan'
import {
  LOANS_FETCH_REQUESTED,
  LOANS_FETCHASKED_REQUESTED
} from '../actions/loanCalculation'
import {
  LOANINQUERY_SAVE_REQUESTED,
  LOANINQUERY_UPLOAD_FILES,
  LOANINQUERY_DELETE_FILES,
  LOANMESSAGE_SAVE_REQUESTED
} from '../actions/loanInQuery'
import {
  ACCOUNTINGACCOUNT_SAVE_REQUESTED,
  ACCOUNTINGACCOUNTS_FETCH_REQUESTED,
  ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED,
  ACCOUNTINGACCOUNT_UPDATE_REQUESTED,
  ACCOUNTINGACCOUNTS_ACTIVE_FETCH_REQUESTED
} from '../actions/accountingAccount'
import {
  deactivateUser,
  fetchUsers,
  saveUser,
  fetchUser,
  fetchUserFinder
} from './user'

import {
  fetchLoginRequested,
  requestedRecoverPass,
  requestUpdatePassword,
  requestForgetPass,
  requestVerifyToken
} from './login'
import fetchForm from './form'
import {
  deactivateBank,
  fetchBanks,
  saveBank,
  fetchBank,
  fetchBankFinder
} from './bank'

import {
  deactivateOrganism,
  fetchOrganisms,
  saveOrganism,
  fetchOrganism,
  fetchOrganismFinder
} from './organism'
import fetchLocationFinder from './location'
import fetchProvinceFinder from './province'
import {
  deactivateCompany,
  fetchCompaniesFinder,
  fetchCompany,
  fetchCompanyes,
  saveCompany
} from './company'
import {
  fetchNosisPerson,
  fetchPerson,
  savePerson,
  fetchPersons,
  deactivatePerson
} from './Person'
import { fetchRole, saveRole, fetchRoles, deactivateRole } from './role'
import {
  saveQuery,
  fetchQuery,
  fetchLoan,
  fetchAskedLoan,
  saveLoanInQuery,
  newQuery,
  fetchQuerys,
  uploadFileQuery,
  deleteFileQuery,
  saveMessage,
  sendByEmail
} from './query'

import { deactivatePlan, fetchPlan, fetchPlans, savePlan } from './plan'

import {
  deactivateAccountingAccount,
  fetchAccountingAccounts,
  saveAccountingAccount,
  fetchAccountingAccount,
  fetchAccountingAccountFinder
} from './accountingAccount'

import { fetchConfigQuerys, saveConfigQuerys } from './configQuery'
import {
  MAILTEMPLATE_DEACTIVATE_REQUESTED,
  MAILTEMPLATE_SAVE_REQUESTED,
  MAILTEMPLATE_UPDATE_REQUESTED,
  MAILTEMPLATES_FETCH_REQUESTED
} from '../actions/mailTemplate'
import {
  deactivateMailTemplate,
  fetchMailTemplates,
  saveMailTemplate,
  fetchMailTemplate
} from './mailTemplate'
import {
  REPORT_QUERIES_DEACTIVATE_REQUESTED,
  REPORT_QUERIES_SAVE_REQUESTED,
  REPORT_QUERIESS_FETCH_REQUESTED
} from '../actions/reportsQueries'
import {
  deactivateReportQueries,
  fetchReportQueries,
  fetchReportQueriess,
  saveReportQueries
} from './reportsQueries'

export default function* root() {
  yield all([
    // Login
    takeEvery(FETCH_LOGIN_REQUESTED, fetchLoginRequested),
    takeEvery(REQUEST_UPDATE_PASS, requestUpdatePassword),
    takeEvery(REQUEST_VERIFY_TOKEN, requestVerifyToken),
    takeEvery(REQUEST_RECOVER_PASS, requestedRecoverPass),
    takeEvery(REQUESTED_FORGET_PASS, requestForgetPass),
    // Form
    takeEvery(FORM_FETCH_REQUESTED, fetchForm),
    // Usuer
    takeEvery(USER_DEACTIVATE_REQUESTED, deactivateUser),
    takeEvery(USERS_FETCH_REQUESTED, fetchUsers),
    takeEvery(USER_SAVE_REQUESTED, saveUser),
    takeEvery(USER_UPDATE_REQUESTED, fetchUser),

    // AccountingAccount
    takeEvery(
      ACCOUNTINGACCOUNT_DEACTIVATE_REQUESTED,
      deactivateAccountingAccount
    ),
    takeEvery(ACCOUNTINGACCOUNTS_FETCH_REQUESTED, fetchAccountingAccounts),
    takeEvery(ACCOUNTINGACCOUNT_SAVE_REQUESTED, saveAccountingAccount),
    takeEvery(ACCOUNTINGACCOUNT_UPDATE_REQUESTED, fetchAccountingAccount),

    // Bank
    takeEvery(BANK_DEACTIVATE_REQUESTED, deactivateBank),
    takeEvery(BANKS_FETCH_REQUESTED, fetchBanks),
    takeEvery(BANK_SAVE_REQUESTED, saveBank),
    takeEvery(BANK_UPDATE_REQUESTED, fetchBank),

    // Company
    takeEvery(COMPANY_DEACTIVATE_REQUESTED, deactivateCompany),
    takeEvery(COMPANIES_FETCH_REQUESTED, fetchCompanyes),
    takeEvery(COMPANY_SAVE_REQUESTED, saveCompany),
    takeEvery(COMPANY_UPDATE_REQUESTED, fetchCompany),

    // finders
    takeEvery(LOCATIONS_ACTIVE_FETCH_REQUESTED, fetchLocationFinder),
    takeEvery(PROVINCES_ACTIVE_FETCH_REQUESTED, fetchProvinceFinder),
    takeEvery(BANKS_ACTIVE_FETCH_REQUESTED, fetchBankFinder),
    takeEvery(ORGANISMS_ACTIVE_FETCH_REQUESTED, fetchOrganismFinder),
    takeEvery(
      ACCOUNTINGACCOUNTS_ACTIVE_FETCH_REQUESTED,
      fetchAccountingAccountFinder
    ),
    takeEvery(USER_ACTIVE_FETCH_REQUESTED_FIND, fetchUserFinder),
    takeEvery(COMPANIES_ACTIVE_FETCH_REQUESTED, fetchCompaniesFinder),

    // Organism
    takeEvery(ORGANISMS_FETCH_REQUESTED, fetchOrganisms),
    takeEvery(ORGANISM_SAVE_REQUESTED, saveOrganism),
    takeEvery(ORGANISM_DEACTIVATE_REQUESTED, deactivateOrganism),
    takeEvery(ORGANISM_UPDATE_REQUESTED, fetchOrganism),

    // Plan
    takeEvery(PLAN_DEACTIVATE_REQUESTED, deactivatePlan),
    takeEvery(PLANS_FETCH_REQUESTED, fetchPlans),
    takeEvery(PLAN_SAVE_REQUESTED, savePlan),
    takeEvery(PLAN_UPDATE_REQUESTED, fetchPlan),

    // Query Person
    takeEvery(PERSON_FETCH_REQUESTED, fetchPerson),
    takeEvery(PERSONS_NOSIS_FETCH_REQUESTED, fetchNosisPerson),
    takeEvery(QUERY_FETCH_REQUESTED, fetchQuery),
    takeEvery(QUERY_SAVE_REQUESTED, saveQuery),
    takeEvery(LOANS_FETCH_REQUESTED, fetchLoan),
    takeEvery(LOANS_FETCHASKED_REQUESTED, fetchAskedLoan),
    takeEvery(LOANINQUERY_SAVE_REQUESTED, saveLoanInQuery),
    takeEvery(QUERY_NEW_REQUESTED, newQuery),
    takeEvery(QUERIES_FETCH_REQUESTED, fetchQuerys),
    takeEvery(LOANINQUERY_UPLOAD_FILES, uploadFileQuery),
    takeEvery(LOANINQUERY_DELETE_FILES, deleteFileQuery),
    takeEvery(LOANMESSAGE_SAVE_REQUESTED, saveMessage),
    takeEvery(QUERY_SEND_BY_EMAIL, sendByEmail),

    // Config Query
    takeEvery(CONFIGQUERY_FETCH_REQUESTED, fetchConfigQuerys),
    takeEvery(CONFIGQUERY_SAVE_REQUESTED, saveConfigQuerys),

    // Mail Template
    takeEvery(MAILTEMPLATE_DEACTIVATE_REQUESTED, deactivateMailTemplate),
    takeEvery(MAILTEMPLATE_UPDATE_REQUESTED, fetchMailTemplate),
    takeEvery(MAILTEMPLATES_FETCH_REQUESTED, fetchMailTemplates),
    takeEvery(MAILTEMPLATE_SAVE_REQUESTED, saveMailTemplate),
    // report Queries
    takeEvery(REPORT_QUERIES_DEACTIVATE_REQUESTED, deactivateReportQueries),
    takeEvery(REPORT_QUERIESS_FETCH_REQUESTED, fetchReportQueries),
    takeEvery(REPORT_QUERIESS_FETCH_REQUESTED, fetchReportQueriess),
    takeEvery(REPORT_QUERIES_SAVE_REQUESTED, saveReportQueries),
    // Person
    takeEvery(PERSONS_FETCH_REQUESTED, fetchPersons),
    takeEvery(PERSON_UPDATE_REQUESTED, fetchPerson),
    takeEvery(PERSON_SAVE_REQUESTED, savePerson),
    takeEvery(PERSON_DEACTIVATE_REQUESTED, deactivatePerson),

    // Role
    takeEvery(ROLES_FETCH_REQUESTED, fetchRoles),
    takeEvery(ROLE_UPDATE_REQUESTED, fetchRole),
    takeEvery(ROLE_SAVE_REQUESTED, saveRole),
    takeEvery(ROLE_DEACTIVATE_REQUESTED, deactivateRole)
  ])
}
