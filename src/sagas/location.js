import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import LocationService from '../services/location'
import { receiveLocationFinded } from '../actions/location'

export default function* fetchLocationFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      LocationService.fetchOnlyActive,
      token,
      fvalue,
      fname
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: `${option.name} - cp: ${option.cp} - ${option.province.name}`,
          value: option.id
        })
      )
      yield put(receiveLocationFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
      if (message === 'No hay localidades') {
        const newLocationJs = [
          {
            label: 'Crear una nueva',
            value: 'new'
          }
        ]
        yield put(receiveLocationFinded(newLocationJs))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
