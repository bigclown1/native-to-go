import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'

import MailTemplateService from '../services/mailTemplate'
import {
  mailTemplateRequestedFetch,
  mailTemplateSaveSucceeded,
  receiveMailTemplate,
  receiveMailTemplates
} from '../actions/mailTemplate'

export function* fetchMailTemplate({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      MailTemplateService.fetch,
      token,
      id,
      'Id',
      'like'
    )
    if (!message) {
      yield put(receiveMailTemplate(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchMailTemplates({
  fvalue,
  fname,
  current,
  columnOrder,
  orderby
}) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      MailTemplateService.fetch,
      token,
      fvalue,
      fname,
      null,
      null,
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveMailTemplates(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateMailTemplate({ id, name, body, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsMailTemplate = {
      id,
      name,
      body
    }
    if (active === 'N') {
      Object.assign(jsMailTemplate, {
        active: 'S'
      })
    } else {
      Object.assign(jsMailTemplate, {
        active: 'N'
      })
    }
    const serviceUpdateMailTemplate = yield call(
      MailTemplateService.updateMailTemplate,
      token,
      id,
      jsMailTemplate
    )
    if (serviceUpdateMailTemplate.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateMailTemplate.message
        })
      )
    } else {
      yield put(mailTemplateRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveMailTemplate({ name, body, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddMailTemplate
    if (actionType === 'update') {
      const id = yield select(state => state.mailTemplate.result[0].id)
      const jsPlan = {
        id,
        name,
        body,
        active: 'S'
      }
      serviceAddMailTemplate = yield call(
        MailTemplateService.updateMailTemplate,
        token,
        id,
        jsPlan
      )
    } else {
      const jsPlan = {
        name,
        body
      }
      serviceAddMailTemplate = yield call(
        MailTemplateService.saveMailTemplate,
        token,
        jsPlan
      )
    }
    if (serviceAddMailTemplate.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddMailTemplate.message
        })
      )
    } else {
      yield put(mailTemplateSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
