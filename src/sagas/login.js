import { call, put } from 'redux-saga/effects'
import UserService from '../services/user'
import {
  receiveLockedUser,
  receiveVerifyToken,
  requestLoginSucceeded,
  requestUpdatePasswordSucceeded,
  requestRecoverPassSucceeded
} from '../actions/login'
import { anErrorOccurred, receiveSession } from '../actions'
import User from '../model/user'

export function* fetchLoginRequested({ user, password }) {
  try {
    const token = yield call(UserService.login, user, password)
    if (token.token === 'UserLocked') {
      yield put(receiveLockedUser())
    } else {
      const profile = yield call(UserService.decodetoken, token.token)
      yield put(
        receiveSession(
          new User(profile),
          token.token,
          token.menuRoutesAll,
          token.routeAll
        )
      )
      yield put(requestLoginSucceeded())
    }
  } catch (err) {
    yield put(
      anErrorOccurred({ anErrorOccurred: true, errorMsg: 'otro error' })
    )
    yield put(receiveSession(new User()))
  }
}

export function* requestedRecoverPass({ user, email }) {
  try {
    const name = user
    const jsUser = {
      name,
      email
    }
    const { message } = yield call(UserService.recoverPass, user, jsUser)
    if (!message) {
      yield put(
        requestRecoverPassSucceeded(
          'Se envió un mesajes a tu correo electronico con las instruciones para recuperar la contraseña'
        )
      )
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* requestVerifyToken({ token }) {
  try {
    const profile = yield call(UserService.decodetoken, token)
    const user = new User(profile)
    const { result, message } = yield call(
      UserService.fetch,
      token,
      user.jti,
      'Id',
      'like'
    )
    if (!message) {
      yield put(receiveVerifyToken(token.token, user.jti, result[0].role))
    } else {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: 'el token no es valido'
        })
      )
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* requestUpdatePassword({ token, user, password, role }) {
  try {
    const id = user
    const pass = password
    const locked = 'N'
    const jsUser = {
      role,
      id,
      pass,
      locked
    }
    const serviceAddUser = yield call(
      UserService.updateUser,
      token,
      user,
      jsUser
    )
    if (serviceAddUser.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddUser.message
        })
      )
    } else {
      yield put(requestUpdatePasswordSucceeded('Se guardo su nueva contraseña'))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* requestForgetPass() {
  try {
    yield put(receiveLockedUser())
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
