import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  userSaveSucceeded,
  receiveUsers,
  userRequestedFetch,
  receiveUser,
  receiveUserFinded
} from '../actions/user'
import UserService from '../services/user'

export function* fetchUser({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      UserService.fetch,
      token,
      id,
      'Id',
      'like'
    )
    if (!message) {
      yield put(receiveUser(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchUsers({ fvalue, current, columnOrder, orderby }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      UserService.fetch,
      token,
      fvalue,
      null,
      null,
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receiveUsers(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivateUser({ id, role, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsUser = {
      id,
      role
    }
    if (active === 'N') {
      Object.assign(jsUser, {
        active: 'S'
      })
    } else {
      Object.assign(jsUser, {
        active: 'N'
      })
    }
    const serviceUpdateUser = yield call(
      UserService.updateUser,
      token,
      id,
      jsUser
    )
    if (serviceUpdateUser.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdateUser.message
        })
      )
    } else {
      yield put(userRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchUserFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      UserService.fetch,
      token,
      fvalue,
      fname,
      null,
      null,
      null,
      null
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.name,
          value: option.id
        })
      )
      yield put(receiveUserFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
      if (message === 'No hay usuarios') {
        const newUserJs = [
          {
            label: 'Sin Asignar',
            value: ''
          }
        ]
        yield put(receiveUserFinded(newUserJs))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveUser({ name, pass, email, role, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddUser
    if (actionType === 'update') {
      const id = yield select(state => state.user.result[0].id)
      const jsUser = {
        id,
        name,
        role,
        pass,
        email,
        active: 'S'
      }
      serviceAddUser = yield call(UserService.updateUser, token, id, jsUser)
    } else {
      serviceAddUser = yield call(
        UserService.saveUser,
        token,
        name,
        pass,
        email,
        role
      )
    }
    if (serviceAddUser.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddUser.message
        })
      )
    } else {
      yield put(userSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
