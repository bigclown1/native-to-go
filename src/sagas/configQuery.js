import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import ConfigQueryService from '../services/configQuery'
import {
  receiveConfigQuerys,
  configQuerySaveSucceeded
} from '../actions/configQuery'
import { addSucceeded, requestAdd } from '../actions/abmStatus'

export function* fetchConfigQuerys() {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const configQuerys = {}

    // CAPACIDAD !!
    let configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'lowCapacity',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'lowCapacity',
        '5'
      )
      configQuerys.lowCapacity = configQuery.value
    } else {
      configQuerys.lowCapacity = configQuery.result[0].value
    }

    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'mediumCapacity',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'mediumCapacity',
        '10'
      )
      configQuerys.mediumCapacity = configQuery.value
    } else {
      configQuerys.mediumCapacity = configQuery.result[0].value
    }

    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'highCapacity',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'highCapacity',
        '20'
      )
      configQuerys.highCapacity = configQuery.value
    } else {
      configQuerys.highCapacity = configQuery.result[0].value
    }

    // SOBREGIRO
    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'highOverdraft',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'highOverdraft',
        '1'
      )
      configQuerys.highOverdraft = configQuery.value
    } else {
      configQuerys.highOverdraft = configQuery.result[0].value
    }

    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'normalOverdraft',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'normalOverdraft',
        '5'
      )
      configQuerys.normalOverdraft = configQuery.value
    } else {
      configQuerys.normalOverdraft = configQuery.result[0].value
    }

    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'lowOverdraft',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'lowOverdraft',
        '10'
      )
      configQuerys.lowOverdraft = configQuery.value
    } else {
      configQuerys.lowOverdraft = configQuery.result[0].value
    }

    configQuery = yield call(
      ConfigQueryService.fetch,
      token,
      'depositPercentage',
      'id',
      'like',
      'A'
    )
    if (configQuery.message === 'No hay configuracion') {
      configQuery = yield call(
        ConfigQueryService.saveConfigQuery,
        token,
        'depositPercentage',
        '32'
      )
      configQuerys.depositPercentage = configQuery.value
    } else {
      configQuerys.depositPercentage = configQuery.result[0].value
    }

    if (configQuerys) {
      yield put(receiveConfigQuerys(configQuerys, 1, 1))
    } else {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: 'Error al iniciar los %'
        })
      )
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* saveConfigQuerys({ name, value }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    const jsConfigQuery = {
      id: name,
      value,
      active: 'S'
    }
    const updateConfigQueryResult = yield call(
      ConfigQueryService.updateConfigQuery,
      token,
      name,
      jsConfigQuery
    )

    if (updateConfigQueryResult.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: updateConfigQueryResult.message
        })
      )
    } else {
      const configQuerys = yield select(state => state.configQuery.result)
      configQuerys[name] = value
      yield put(configQuerySaveSucceeded(configQuerys, 1, 1))
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
