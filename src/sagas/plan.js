import { call, put, select } from 'redux-saga/effects'
import { anErrorOccurred, clearError } from '../actions'
import { requestAdd, addSucceeded } from '../actions/abmStatus'
import {
  planSaveSucceeded,
  receivePlans,
  planRequestedFetch,
  receivePlan,
  receivePlanFinded
} from '../actions/plan'
import PlanService from '../services/plan'

export function* fetchPlanFinder({ fvalue, fname }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      PlanService.fetch,
      token,
      fvalue,
      fname,
      null,
      'S'
    )
    if (!message) {
      const newResult = []
      result.map(option =>
        newResult.push({
          label: option.planCode,
          value: option.id
        })
      )
      yield put(receivePlanFinded(newResult))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
      if (message === 'No hay planes') {
        const newPlanJs = [
          {
            label: 'Crear uno nuevo',
            value: 'new'
          }
        ]
        yield put(receivePlanFinded(newPlanJs))
      }
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchPlan({ id }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, message } = yield call(
      PlanService.fetch,
      token,
      id,
      'plan.Id',
      'like',
      'A'
    )
    if (!message) {
      yield put(receivePlan(result))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* fetchPlans({ fvalue, fname, current, columnOrder, orderby }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const { result, size, total, message } = yield call(
      PlanService.fetch,
      token,
      fvalue,
      fname,
      null,
      'R',
      'A',
      current,
      columnOrder,
      orderby
    )
    if (!message) {
      yield put(receivePlans(result, size, total))
    } else {
      yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: message }))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* deactivatePlan({ id, number, active }) {
  yield put(clearError())
  try {
    const token = yield select(state => state.session.token)
    const jsPlan = {
      id,
      number
    }
    if (active === 'N') {
      Object.assign(jsPlan, {
        active: 'S'
      })
    } else {
      Object.assign(jsPlan, {
        active: 'N'
      })
    }
    const serviceUpdatePlan = yield call(
      PlanService.updatePlan,
      token,
      id,
      jsPlan
    )
    if (serviceUpdatePlan.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceUpdatePlan.message
        })
      )
    } else {
      yield put(planRequestedFetch(null))
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}

export function* savePlan({ jsonAdd, actionType }) {
  yield put(clearError())
  try {
    yield put(requestAdd())
    const token = yield select(state => state.session.token)
    let serviceAddPlan
    if (actionType === 'update') {
      const id = yield select(state => state.plan.result[0].id)
      const idOrganism = jsonAdd.idOrganism ? jsonAdd.idOrganism.value : null
      const jsPlan = {
        id,
        planCode: jsonAdd.planCode,
        description: jsonAdd.description,
        scoreFrom: jsonAdd.scoreFrom,
        fees: jsonAdd.fees,
        scoreTo: jsonAdd.scoreTo,
        maxAmount: jsonAdd.maxAmount,
        maxTerm: jsonAdd.maxTerm,
        isNew: jsonAdd.isNew,
        laborOld: jsonAdd.laborOld,
        maxQBanksUntilSituation5: jsonAdd.maxQBanksUntilSituation5,
        administrativeExpenses: jsonAdd.administrativeExpenses,
        creationDate: jsonAdd.creationDate,
        expirationType: jsonAdd.expirationType,
        maxNumberCommercial: jsonAdd.maxNumberCommercial,
        iniDia1: jsonAdd.iniDia1,
        finDia1: jsonAdd.finDia1,
        iniDia2: jsonAdd.iniDia2,
        finDia2: jsonAdd.finDia2,
        worstBankSituation: jsonAdd.worstBankSituation,
        organism: {
          id: idOrganism
        },
        active: 'S'
      }
      serviceAddPlan = yield call(PlanService.updatePlan, token, id, jsPlan)
    } else {
      serviceAddPlan = yield call(PlanService.savePlan, token, jsonAdd)
    }
    if (serviceAddPlan.message) {
      yield put(
        anErrorOccurred({
          anErrorOccurred: true,
          errorMsg: serviceAddPlan.message
        })
      )
    } else {
      yield put(planSaveSucceeded())
      yield put(addSucceeded())
    }
  } catch (err) {
    yield put(anErrorOccurred({ anErrorOccurred: true, errorMsg: err }))
  }
}
