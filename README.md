# react-native-boilerplate

## What is React Native Boilerplate
 It is a template that you can clone and reuse for every project. It is starting point for React Native application.

## React Native Boilerplate

consist of:

- react-native": "0.63.4"
- react-navigation and its dependencies
- redux, redux persist and redux thunk
- react native vector icons
- react-native async storage

## Getting Started

1. Clone this repo, `git clone https://github.com/handi-dev/react-native-boilerplate.git <your project name>`
2. Go to project's root directory, `cd <your project name>`
3. Remove `.git` folder, `rm -rf .git`
4. Use [React Native Rename](https://github.com/junedomingo/react-native-rename) to update project name `$ npx react-native-rename <newName>`
5. Run `npm install` to install dependencies
6. Run `npx pod-install` from root of your project (iOS only)
7. Start the packager with `npm start`
8. Connect your device or use emulator that's installed in your pc
9. Run the test application:

- On Android:
  - Run `npx react-native run-android`
- On iOS:
  - Open `npx react-native run-ios`


-Android Redux-remote-devtools
In terminal, in the project directory, run
    1- emulator -avd <device_name> or start emulator using Android Studio /// On divice, make sure (on terminal) adb devices -watches for your external divice
    2- react-native start --reset-cache  
    3- npm run remotedev //to start the socket server
    4- adb reverse tcp:8000 tcp:8000 //bind the remotedev server into the emulator or divice 
    5- adb reverse tcp:8081 tcp:8081 //bind the devTool server into the emulator or divice
    6- npx react-native run-android //run
    7- after app opens, press d on the node console an open the
        dev menu on the app this will open a chrome tab,
        make sure the Status: Debugger session #0 active. if not, reload the app.
        Right click and open redux dev tools extension for google chrome,
        Chose remote from the bottom panel, settings, use custom local server
        with hostname as localhost and port as 8000



## List of Q & A:

  #### I got error `Error: spawn ./gradlew EACCES` when run `npx react-native run-android`.
  Run this command `chmod 755 android/gradlew` from your root project directory

  #### I got error `Error: SDK location not found. Define location with sdk.dir in the local.properties file or with an ANDROID_HOME environment variable.`
  - Go to your Project -> Android
  - Create a file local.properties
  - Open the file
  - Paste your Android SDK path depending on the operating system:

    - Windows:
      sdk.dir=C:/Users/`USERNAME`/AppData/Local/Android/sdk
    - Linux or MacOS
      sdk.dir=/home/`USERNAME`/Android/sdk

  - Replace `USERNAME` with your PC username
